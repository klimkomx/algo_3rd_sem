#include <cstring>
#include <iostream>
#include <vector>
#include <algorithm>


const int ALPH_SIZE = 27;

int get_idx(char c) {
    if (c == '$')
        return 0;
    return c - 'a' + 1;
}

class suffixTree {
public:
    suffixTree(): vertexes(1)
    {
    }
    ~suffixTree() {}
    struct vertex {
        int prev,
        suf_link,
        from,
        to, min, max;
        bool is_list;
        int next[ALPH_SIZE];
        vertex() {
            prev = suf_link =
                    from =
                            to = -1;
            suf_link = 0;
            for (int i = 0; i < ALPH_SIZE; ++i) {
                next[i] = -1;
            }
            min = 10000000;
            max = -1;
            is_list = false;
        }
        ~vertex(){}
    };

    struct tree_position {
        int vt_num, offset;
        tree_position() = default;
        tree_position(int vt, int off): vt_num(vt),
        offset(off){}
    };

    tree_position next_vt(tree_position pos, const std::string& str, int sufnum) {
        if (vertexes[pos.vt_num].to != pos.offset) {
            if (str[sufnum] == str[pos.offset + 1]) {
                ++pos.offset;
                return pos;
            } else
                return tree_position(-1, -1);
        }
        int vtnum = pos.vt_num;
        int next_on_edge = vertexes[vtnum].next[get_idx(str[sufnum])];
        if (next_on_edge == -1)
            return tree_position(-1, -1);
        else
            return tree_position(next_on_edge, vertexes[next_on_edge].from);
    }

    tree_position get_suf_link(tree_position pos, const std::string& str) {
        if (vertexes[pos.vt_num].prev == 0) {
            if (vertexes[pos.vt_num].from == pos.offset)
                return tree_position(0, -1);
            else {
                int next_vert = vertexes[0].next[get_idx(str[vertexes[pos.vt_num].from + 1])];
                tree_position res(next_vert, vertexes[next_vert].from);
                for (int i = pos.offset - vertexes[pos.vt_num].from; i > 0;) {
                    if (vertexes[res.vt_num].to - vertexes[res.vt_num].from + 1 >= i) {
                        res.offset += i - 1;
                        break;
                    } else {
                        i -= vertexes[res.vt_num].to - vertexes[res.vt_num].from + 1;
                        res.vt_num = vertexes[res.vt_num].next[get_idx(str[pos.offset - i + 1])];
                        res.offset = vertexes[res.vt_num].from;
                    }
                }
                return res;
            }
        }


        int prev_link = vertexes[vertexes[pos.vt_num].prev].suf_link;
        int next_vert = vertexes[prev_link].next[get_idx(str[vertexes[pos.vt_num].from])];

        tree_position res(next_vert, vertexes[next_vert].from);
        for (int i = pos.offset - vertexes[pos.vt_num].from + 1; i > 0;) {
            if (vertexes[res.vt_num].to - vertexes[res.vt_num].from + 1 >= i) {
                res.offset += i - 1;
                break;
            } else {
                i -= vertexes[res.vt_num].to - vertexes[res.vt_num].from + 1;
                res.vt_num = vertexes[res.vt_num].next[get_idx(str[pos.offset - i + 1])];
                res.offset = vertexes[res.vt_num].from;
            }
        }
        return res;
    }

    void ukkonen(std::string& str) {
        tree_position curr(0, -1);
        int curr_saved = -1;
        for (int i = 0; i < str.size(); ++i) {
            curr_saved = -1;

            while (curr.vt_num != -1) {
                auto position = next_vt(curr, str, i);
                if (position.vt_num == -1) {
                    if (curr.offset == vertexes[curr.vt_num].to) {
                        if (curr_saved != -1) {
                            vertexes[curr_saved].suf_link = curr.vt_num;
                            curr_saved = -1;
                        }
                        vertexes.emplace_back();
                        int newlist = vertexes.size() - 1;
                        vertexes[newlist].from = i;
                        vertexes[newlist].to = str.size() - 1;
                        vertexes[newlist].prev = curr.vt_num;
                        vertexes[newlist].is_list = true;
                        vertexes[curr.vt_num].next[get_idx(str[i])] = newlist;

                    } else {
                        if (curr_saved != -1) {
                            vertexes[curr_saved].suf_link = vertexes.size();
                            curr_saved = -1;
                        }
                        vertexes.emplace_back();
                        vertexes.emplace_back();
                        int newvert = vertexes.size() - 2,
                        newlist = vertexes.size() - 1;
                        int from = vertexes[curr.vt_num].prev;
                        vertexes[from].next[get_idx(str[vertexes[curr.vt_num].from])] =
                                newvert;
                        vertexes[newvert].suf_link = vertexes.size();
                        vertexes[newvert].prev = from;
                        vertexes[newvert].from = vertexes[curr.vt_num].from;
                        vertexes[newvert].to = curr.offset;
                        vertexes[newvert].next[get_idx(str[curr.offset +  1])] = curr.vt_num;
                        vertexes[newvert].next[get_idx(str[i])] = newlist;

                        vertexes[curr.vt_num].from = curr.offset + 1;
                        vertexes[curr.vt_num].prev = newvert;
                        vertexes[newlist].prev = newvert;
                        vertexes[newlist].from = i;
                        vertexes[newlist].to = str.size() - 1;
                        vertexes[newlist].is_list = true;
                        curr = tree_position(newvert, curr.offset);
                    }
                } else {
                    if (curr_saved != -1)
                        vertexes[curr_saved].suf_link = curr.vt_num;
                    curr = position;
                    break;
                }
                if (curr.vt_num == 0) {
                    if (curr_saved != -1) {
                        vertexes[curr_saved].suf_link = -1;
                        curr_saved = -1;
                    }

                    break;
                }
                curr_saved = curr.vt_num;
                curr = get_suf_link(curr, str);
            }
        }
        return;
    }

    void find_vt(long long& cur_max,
                 long long length = 0ll, int vt = 0) {
        if (vt != 0)
            length += vertexes[vt].to - vertexes[vt].from + 1 - vertexes[vt].is_list;

        if (vertexes[vt].is_list)
            vertexes[vt].min = vertexes[vt].max = length;
        for (int i = 0, vt_to; i < ALPH_SIZE; ++i) {
            vt_to = vertexes[vt].next[i];
            if (vt_to != -1) {
                find_vt(cur_max, length, vt_to);
                vertexes[vt].min = std::min(vertexes[vt].min,
                                            vertexes[vt_to].min);
                vertexes[vt].max = std::max(vertexes[vt].max,
                                            vertexes[vt_to].max);
            }
        }
        if (vertexes[vt].min != vertexes[vt].max &&
            length * length + vertexes[vt].max - vertexes[vt].min +
            length > cur_max) {
            cur_max = length * length + vertexes[vt].max - vertexes[vt].min +
                    length;
        }
    }

    long long solve(const std::string& str) {

        long long max_val = 0;
        find_vt(max_val);

        return max_val;
    }
private:
    std::vector<vertex> vertexes;

};

int main() {
    std::cin.tie(0);
    std::ios::sync_with_stdio(0);

    std::string s;
    std::cin >> s;
    s += '$';

    suffixTree tree;
    tree.ukkonen(s);

    std::cout << tree.solve(s);
    return 0;
}