#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <queue>
#include <unordered_map>

///////////////////trie
const int ALPH_SIZE = 2;

static inline int get_idx(char i) {
    return i == '1';
}

static inline char get_by_idx(int i) {
    return (i + '0');
}

class trie {
private:
    struct vertex {
        uint32_t to[ALPH_SIZE];
        uint32_t suffix_link;
        uint32_t output_link;
        uint32_t parent;
        int par_symb_idx;
        int str_num_if_term;

        vertex(uint32_t parent, int idx, int num = 0): suffix_link(UINT32_MAX),
        output_link(0),
        parent(parent),
        par_symb_idx(idx),
        str_num_if_term(num) {
            for (int i = 0; i < ALPH_SIZE; ++i) {
                to[i] = UINT32_MAX;
            }
        }
    };

    std::vector<vertex> storage;
    int max_str_num = 0;

    int calc_jump(uint32_t vert_idx, int symb_idx) {
        if (storage[vert_idx].to[symb_idx] != UINT32_MAX)
            return storage[vert_idx].to[symb_idx];
        else if (vert_idx == 0)
            return storage[vert_idx].to[symb_idx] = 0;
        return storage[vert_idx].to[symb_idx] = calc_jump(storage[vert_idx].suffix_link, symb_idx);
    }

    bool dfs(int idx, std::vector<int>& color) {
        if (storage[idx].str_num_if_term ||
            storage[idx].output_link != 0 || color[idx] == 2) {
            color[idx] = 2;
            return false;
        }
        if (color[idx] == 1)
            return true;

        color[idx] = 1;

        for (int i = 0; i < ALPH_SIZE; ++i) {
            if (calc_jump(idx, i) == 0 || dfs(calc_jump(idx, i), color))
                return true;
        }

        color[idx] = 2;
        return false;
    }
public:
    trie(): storage(1, vertex(0, 0)) {}
    void insert(const std::string& str) {

        uint32_t current_idx = 0;
        int idx;
        for (int i = 0; i < str.size(); ++i) {
            idx = get_idx(str[i]);
            if (storage[current_idx].to[idx] == UINT32_MAX)  {
                storage.emplace_back(current_idx, idx);
                storage[current_idx].to[idx] = storage.size() - 1;
            }
            current_idx = storage[current_idx].to[idx];
        }
        if (!storage[current_idx].str_num_if_term) {
            storage[current_idx].str_num_if_term = ++max_str_num;
        }
    }

    static trie aho_corasick(const std::vector<std::string>& dict) {
        trie automata;
        for (int i = 0; i < dict.size(); ++i) {
            automata.insert(dict[i]);
        }
        automata.storage[0].suffix_link = 0;

        std::queue<uint32_t> order;
        for (int i = 0; i < ALPH_SIZE; ++i) {
            if (automata.storage[0].to[i] != UINT32_MAX) {
                automata.storage[automata.storage[0].to[i]].suffix_link = 0;
                for (int j = 0; j < ALPH_SIZE; ++j) {
                    if (automata.storage[
                            automata.storage[0].to[i]].to[j] != UINT32_MAX) {
                        order.push(automata.storage[
                                automata.storage[0].to[i]].to[j]);
                    }
                }

            }
        }

        uint32_t idx;
        uint32_t parent_link;
        while (!order.empty()) {
            idx = order.front();
            order.pop();
            parent_link = automata.storage[automata.storage[idx].parent].suffix_link;
            while (automata.storage[idx].suffix_link == UINT32_MAX) {
                automata.storage[idx].suffix_link =
                        automata.storage[parent_link]
                        .to[automata.storage[idx].par_symb_idx];
                if (parent_link == 0 &&
                automata.storage[idx].suffix_link == UINT32_MAX) {
                    automata.storage[idx].suffix_link = 0;
                    break;
                }
                parent_link = automata.storage[parent_link].suffix_link;
            }
            if (automata.storage[automata.storage[idx].
            suffix_link].str_num_if_term) {
                automata.storage[idx].output_link =
                        automata.storage[idx].suffix_link;
            } else {
                automata.storage[idx].output_link =
                        automata.storage[automata.storage[idx].
                        suffix_link].output_link;
            }
            for (int i = 0; i < ALPH_SIZE; ++i) {
                if (automata.storage[idx].to[i] != UINT32_MAX)
                    order.push(automata.storage[idx].to[i]);
            }
        }
        return automata;
    }




    static std::vector<std::vector<int>> find_all_entries(trie& automata,
                                                          const std::string& text) {
        std::vector<std::vector<int>> res(automata.max_str_num);

        uint32_t curr_state = 0, copy;
        for (int i = 0; i < text.size(); ++i) {
            curr_state = automata.calc_jump(curr_state, get_idx(text[i]));
            copy = curr_state;

            while (copy) {
                if (automata.storage[copy].str_num_if_term) {
                    res[automata.storage[copy].str_num_if_term - 1].push_back(i + 2);
                }
                copy = automata.storage[copy].output_link;
            }
        }
        return res;
    }

    std::string solve() {
        std::vector<int> used(storage.size());
        return (dfs(0, used))? "TAK" : "NIE";
    }
};


//////////////////////////////



int main() {
    std::ios::sync_with_stdio(0);
    std::cin.tie(0);

    std::vector<std::string> dict;
    std::unordered_map<std::string, int> string_nums;

    int num, cnt = 0, strnum;

    std::cin >> num;
    dict.resize(num);

    for (auto& pattern : dict) {
        std::cin >> pattern;
    }

    auto automata = trie::aho_corasick(dict);
    std::cout << automata.solve();
    return 0;
}