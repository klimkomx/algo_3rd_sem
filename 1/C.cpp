#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <queue>
#include <unordered_map>

///////////////////trie

const int ALPH_SIZE = 26;

static inline int get_idx(char i) {
    return (i - 'a');
}

static inline char get_by_idx(int i) {
    return (i + 'a');
}

class trie {
public:
    struct vertex {
        uint32_t to[ALPH_SIZE];
        int8_t is_term;
        std::vector<int> dna_ind;
        std::vector<int> pos;

        vertex( bool is_tm):
        is_term(is_tm)
        {
            for (int i = 0; i < ALPH_SIZE; ++i) {
                to[i] = UINT32_MAX;
            }
        }
    };

    std::vector<vertex> storage;
public:
    trie(): storage(1, vertex(0)) {}
    std::vector<int> insert(const std::string& str, const std::vector<std::string>& dna) {

        uint32_t current_idx = 0;
        int idx;
        uint32_t vt_rem = 0;
        bool wup = 0;
        for (int i = 0; i < str.size(); ++i) {
            idx = get_idx(str[i]);
            if (storage[current_idx].to[idx] == UINT32_MAX)  {
                storage.emplace_back(0);
                storage[current_idx].to[idx] = storage.size() - 1;
                if (!wup)
                    vt_rem = current_idx, wup = 1;
            }
            current_idx = storage[current_idx].to[idx];
        }
        storage[current_idx].is_term = 1;
        return upd(vt_rem, dna);
    }

    std::vector<int> add_dna(int dnanum, const std::vector<std::string>& dna) {
        int pos = 0;
        std::vector<int> ans;
        int vt_c = 0,
            idx = get_idx(dna[dnanum][pos]);
        while (pos < dna[dnanum].size() && storage[vt_c].to[idx] != UINT32_MAX) {
            vt_c = storage[vt_c].to[idx];
            ++pos;
            if (storage[vt_c].is_term)
                vt_c = 0;
            if (pos == dna[dnanum].size())
                break;
            idx = get_idx(dna[dnanum][pos]);
        }
        if (pos == dna[dnanum].size() && vt_c == 0)
            ans.push_back(dnanum);
        else
            storage[vt_c].dna_ind.push_back(dnanum),
            storage[vt_c].pos.push_back(pos);
        return ans;
    }

    std::vector<int> upd(int vt_num, const std::vector<std::string>& dna) {
        std::vector<int> dna_cp = std::move(storage[vt_num].dna_ind);
        std::vector<int> pos_cp = std::move(storage[vt_num].pos), ans;
        int vt_c = vt_num, idx;
        for (int i = 0; i < dna_cp.size(); ++i) {
            vt_c = vt_num;
            idx = get_idx(dna[dna_cp[i]][pos_cp[i]]);
            while (pos_cp[i] < dna[dna_cp[i]].size() && storage[vt_c].to[idx] != UINT32_MAX) {
                vt_c = storage[vt_c].to[idx];
                ++pos_cp[i];
                if (storage[vt_c].is_term)
                    vt_c = 0;
                if (pos_cp[i] == dna[dna_cp[i]].size())
                    break;
                idx = get_idx(dna[dna_cp[i]][pos_cp[i]]);
            }
            if (pos_cp[i] == dna[dna_cp[i]].size() && vt_c == 0)
                ans.push_back(dna_cp[i]);
            else
                storage[vt_c].dna_ind.push_back(dna_cp[i]),
                storage[vt_c].pos.push_back(pos_cp[i]);
        }
        return ans;
    }
};


//////////////////////////////

std::string rotate(const std::string& str, int ld) {
    int sz = str.size();
    std::string ans(sz, 'a');
    for (int i = 0; i < str.size(); ++i)
        ans[i] = str[(i + ld) % sz];
    return ans;
}
int32_t main() {
    std::ios::sync_with_stdio(0);
    std::cin.tie(0);

    int q_num, prev = 0;
    trie bor;

    std::vector<std::string> dna;
    std::string inp;
    char ch;

    std::cin >> q_num;

    for (int i = 0; i < q_num; ++i) {
        std::cin >> ch;
        std::cin >> inp;
        inp = rotate(inp, prev);
        if (ch == '?') {
            dna.push_back(std::move(inp));
            auto vt = bor.add_dna(dna.size() - 1, dna);
            prev = vt.size();
            std::cout << vt.size() << " ";
            for (auto& elem : vt)
                std::cout << elem + 1;
            std::cout << "\n";
        }
        else {
            auto vt = bor.insert(inp, dna);
            prev = vt.size();
            std::cout << vt.size() << " ";
            for (auto& elem : vt)
                std::cout << elem + 1 << " ";
            std::cout << "\n";
        }

    }
    return 0;
}