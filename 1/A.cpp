#include <iostream>
#include <vector>
#include <string>
#include <algorithm>


std::vector<size_t> z_function(const std::string& str) {
    std::vector<size_t> z_f(str.size(), 0);
    size_t lb = 0, rb = 0;

    for (size_t i = 0; i < z_f.size(); ++i) {
        if (i < rb)
	    z_f[i] = std::min(z_f[i - lb], rb - i);
	while (i + z_f[i] <= str.size() && str[z_f[i]] == str[z_f[i] + i])
	    ++z_f[i];
	if (i + z_f[i] > rb)
	    rb = i + z_f[i], 
	    lb = i;
    }
    return z_f; 
}

std::vector<size_t> find_number_pos(const std::vector<size_t>& z_f, 
                                          const size_t num) {
    std::vector<size_t> positions;
    for(size_t i = 0; i < z_f.size(); ++i) {
        if (z_f[i] == num)
	    positions.push_back(i);
    }
    return positions;
}
int32_t main() {
    std::string s, t;
    std::cin >> s >> t;
    size_t t_sz = t.size();
    for (const size_t& pos : find_number_pos(z_function(t + "#" + s), t_sz))
        std::cout << pos - t_sz - 1<< " ";
    std::cout << "\n";
    return 0;
}
