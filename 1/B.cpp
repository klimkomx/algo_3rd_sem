#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

template<typename T>
std::vector<size_t> z_function(const std::vector<T> &seq) {
    std::vector<size_t> z_f(seq.size(), 0);
    size_t lb = 0, rb = 0;

    for (size_t i = 1; i < z_f.size(); ++i) {
        if (i < rb) {
            z_f[i] = std::min(z_f[i - lb], rb - i);
        }
        while (i + z_f[i] <= seq.size() && seq[z_f[i]] == seq[z_f[i] + i])
            ++z_f[i];
        if (i + z_f[i] > rb) {
            rb = i + z_f[i];
            lb = i;
        }
    }
    return z_f;
}

std::vector<size_t> solve(std::vector<int> &cubes_seq, size_t n) {
    std::vector<size_t> ans;

    for (int i = n - 1; i >= 0; --i) {
        cubes_seq.push_back(cubes_seq[i]);
    }
    auto z_f = z_function(cubes_seq);
    for (int i = n / 2; i >= 0; --i) {
        if (z_f[2 * n - 2 * i] >= i) {
            ans.push_back(n - i);
        }
    }

    return ans;
}

int32_t main() {
    size_t n, m;
    std::cin >> n >> m;

    std::vector<int> cubes_seq;
    cubes_seq.reserve(2 * n);

    for (int i = 0; i < n; ++i) {
        std::cin >> m;
        cubes_seq.push_back(m);
    }

    for (const size_t &pos : solve(cubes_seq, n))
        std::cout << pos << " ";
    std::cout << "\n";
    return 0;
}
