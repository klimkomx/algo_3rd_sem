//
//int main() {{{{{{{{{{{{{{}}}}}}}}}}}}}}

#include <string>
#include <vector>
#include <algorithm>
#include <iostream>

std::vector<size_t> z_function(const std::string& str) {
    std::vector<size_t> z_f(str.size(), 0);
    size_t lb = 0, rb = 0;

    for (size_t i = 1; i < z_f.size(); ++i) {
        if (i < rb)
            z_f[i] = std::min(z_f[i - lb], rb - i);
        while (i + z_f[i] <= str.size() && str[z_f[i]] == str[z_f[i] + i])
            ++z_f[i];
        if (i + z_f[i] > rb)
            rb = i + z_f[i],
            lb = i;
    }
    return z_f;
}

std::vector<int> solve(std::string& t, std::string& p, int k) {
    auto original = z_function(p + "#" + t);
    std::reverse(p.begin(), p.end());
    std::reverse(t.begin(), t.end());
    auto reversed = z_function(p + "#" + t);

    std::vector<int> ans;

    for (int i = p.size() + 1, j = t.size() + 1;
    i <= t.size() + 1; ++i, --j) {
        if (original[i] + reversed[j] + k >= p.size())
            ans.push_back(i - static_cast<int>(p.size()));
    }
    return ans;
}

int main() {
    std::ios::sync_with_stdio(0);
    std::cin.tie(0);

    std::string t, p;
    int k;

    std::cin >> t >> p >> k;

    auto ans = solve(t, p, k);

    std::cout << ans.size() << "\n";
    for (auto elem : ans) {
        std::cout << elem << " ";
    }
    std::cout << "\n";
    return 0;
}