#include <iostream>
#include <vector>
#include <string>
#include <algorithm>


std::vector<unsigned long long> z_function(const std::string& str, int from) {
    std::vector<unsigned long long> z_f(str.size(), 0ull);
    unsigned long long lb = from, rb = from;

    for (unsigned long long i = from + 1ull; i < z_f.size(); ++i) {
        if (i < rb)
            z_f[i] = std::min(z_f[i - lb + from], rb - i);
        while (i + z_f[i] <= z_f.size() && str[z_f[i] + from] == str[z_f[i] + i])
            ++z_f[i];
        if (i + z_f[i] > rb)
            rb = i + z_f[i],
            lb = i;
    }
    return z_f;
}

unsigned long long cnt_substr(std::string& str) {
    unsigned long long res = str.size() * (str.size() + 1ull) * (str.size() + 2ull) / 6ull;
    std::vector<unsigned long long> collisions(str.size(), 0);
    for (int i = 0; i < str.size(); ++i) {
        auto z_f = z_function(str, i);
        for (int j = 0; j < str.size(); ++j)
            collisions[j] = std::max(collisions[j], z_f[j]);
    }
    for (int i = 0; i < str.size(); ++i)
        res -= collisions[i] * (collisions[i] + 1ull) / 2ull;
    return res;
}

int main() {
    std::ios::sync_with_stdio(0);
    std::cin.tie(0);

    std::string inp;
    std::cin >> inp;


    std::cout << cnt_substr(inp) << "\n";
    return 0;
}