#include <iostream>
#include <vector>
#include <string>
#include <algorithm>


///////////////////trie
const int ALPH_SIZE = 26;

static inline int get_idx(char i) {
    return i - 'a';
}

static inline char get_by_idx(int i) {
    return (i + 'a');
}

class trie {
private:
    struct vertex {
        std::vector<vertex*> to;
//        bool is_term;
        int cnt;
        vertex(bool tm = false): to(ALPH_SIZE, nullptr), cnt(0){}
        ~vertex() {
            for (int i = 0; i < ALPH_SIZE; ++i) {
                if (to[i] != nullptr)
                    delete to[i];
            }
        }
    };
    vertex head;
    void get_all(const vertex* ptr, std::string& pf_now,  std::vector<std::string>& all_str) const {
        for (int i = 0; i < ptr->cnt; ++i)
            all_str.push_back(pf_now);
        for (int i = 0; i <  ALPH_SIZE; ++i) {
            if (ptr->to[i] != nullptr) {
                pf_now.push_back(get_by_idx(i));
                get_all(ptr -> to[i], pf_now, all_str);
                pf_now.pop_back();
            }
        }
    }
public:
    std::vector<std::string> get_content() const {
        std::string get_all_2nd_arg;
        std::vector<std::string> get_all_3rd_arg;
        get_all(&head, get_all_2nd_arg, get_all_3rd_arg);
        return get_all_3rd_arg;
    }

    void insert(const std::string& str) {
        vertex* head_now = &head;
        int idx;
        for (int i = 0; i < str.size(); ++i) {
            idx = get_idx(str[i]);
            head_now = (head_now -> to[idx] == nullptr) ?
                    head_now -> to[idx] = new vertex() : head_now -> to[idx];
        }
        ++head_now -> cnt;
    }

    friend std::ostream& operator<<(std::ostream&, const trie&);
};

std::ostream& operator<<(std::ostream& out, const trie& tr) {
    std::vector<std::string> trie_content;
    std::string base(0);
    tr.get_all(&tr.head, base, trie_content);

    for (const auto& elem : trie_content)
        out << elem << " ";
    out << "\n";
    return out;
}
//////////////////////////////



std::pair<std::vector<std::string>,
std::vector<int>> parse(const std::string& str) {
    std::vector<int> points;
    std::vector<std::string> strings;
    char prevc = (str[0] == '.') ? 'a' : '.';
    for (int i = 0; i < str.size(); ++i) {
        if (str[i] == '.') {
            if (prevc != '.')
                points.push_back(1);
            else
                ++points[points.size() - 1];
        } else {
            if (prevc == '.')
                strings.push_back(std::string(1, str[i]));
            else
                strings[strings.size() - 1].push_back(str[i]);
        }
        prevc = str[i];
    }
    return std::make_pair(strings, points);
}

int main() {
    std::ios::sync_with_stdio(0);
    std::cin.tie(0);


    std::string str;
    std::cin >> str;
    auto splitted_str = parse(str);
    trie sorter;
    for (int i = 0; i < splitted_str.first.size(); ++i)
        sorter.insert(splitted_str.first[i]);
    auto sorted_substr = sorter.get_content();

    int vt_points_ptr = 0,
    vt_strings_ptr = 0;
    bool on_ft = str[0] == '.';


    while (vt_points_ptr < splitted_str.second.size() ||
    vt_strings_ptr < sorted_substr.size()) {
        if (on_ft) {
            for (int i = 0; i < splitted_str.second[vt_points_ptr]; ++i)
                std::cout << '.';
            ++vt_points_ptr;
            on_ft = false;
        } else {
            std::cout << sorted_substr[vt_strings_ptr++];
            on_ft = true;
        }
    }
    std::cout << "\n";
    return 0;
}