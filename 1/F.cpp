#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <queue>
#include <unordered_map>

///////////////////trie

const long long ALPH_SIZE = 26;

static inline long long get_idx(char i) {
    return (i - 'a');
}

static inline char get_by_idx(long long i) {
    return (i + 'a');
}

class trie {
private:
    struct vertex {
        long long to[ALPH_SIZE];
        long long suffix_link;
        long long output_link;
        long long parent;
        long long par_symb_idx;
        long long str_num_if_term;
        long long term_val;
        long long nonterm_val;
        long long tm_copy;
        long long ntm_copy;

        vertex(long long parent, long long idx, long long num = 0) : suffix_link(INT64_MAX),
                                                                     output_link(0),
                                                                     parent(parent),
                                                                     par_symb_idx(idx),
                                                                     str_num_if_term(num),
                                                                     term_val(0),
                                                                     nonterm_val(0) {
            for (long long i = 0; i < ALPH_SIZE; ++i) {
                to[i] = INT64_MAX;
            }
        }
    };

    std::vector<vertex> storage;
    long long max_str_num = 0;

    long long calc_jump(long long vert_idx, long long symb_idx) {
        if (storage[vert_idx].to[symb_idx] != INT64_MAX)
            return storage[vert_idx].to[symb_idx];
        else if (vert_idx == 0)
            return storage[vert_idx].to[symb_idx] = 0;
        return storage[vert_idx].to[symb_idx] = calc_jump(storage[vert_idx].suffix_link, symb_idx);
    }

public:
    trie() : storage(1, vertex(0, 0)) {
        storage[0].nonterm_val = 1;
    }

    void insert(const std::string &str) {

        long long current_idx = 0;
        long long idx;
        for (long long i = 0; i < str.size(); ++i) {
            idx = get_idx(str[i]);
            if (storage[current_idx].to[idx] == INT64_MAX) {
                storage.emplace_back(current_idx, idx);
                storage[current_idx].to[idx] = storage.size() - 1;
                storage[storage.size() - 1].nonterm_val = 0;
            }
            current_idx = storage[current_idx].to[idx];
        }
        if (!storage[current_idx].str_num_if_term) {
            storage[current_idx].str_num_if_term = ++max_str_num;
            storage[current_idx].term_val = 0;
            storage[current_idx].nonterm_val = 0;
        }
    }

    static trie aho_corasick(const std::vector<std::string> &dict) {
        trie automata;
        for (long long i = 0; i < dict.size(); ++i) {
            automata.insert(dict[i]);
        }
        automata.storage[0].suffix_link = 0;

        std::queue<long long> order;
        for (long long i = 0; i < ALPH_SIZE; ++i) {
            if (automata.storage[0].to[i] != INT64_MAX) {
                automata.storage[automata.storage[0].to[i]].suffix_link = 0;
                for (long long j = 0; j < ALPH_SIZE; ++j) {
                    if (automata.storage[
                                automata.storage[0].to[i]].to[j] != INT64_MAX) {
                        order.push(automata.storage[
                                           automata.storage[0].to[i]].to[j]);
                    }
                }

            }
        }

        long long idx;
        long long parent_link;
        while (!order.empty()) {
            idx = order.front();
            order.pop();
            parent_link = automata.storage[automata.storage[idx].parent].suffix_link;
            while (automata.storage[idx].suffix_link == INT64_MAX) {
                automata.storage[idx].suffix_link =
                        automata.storage[parent_link]
                                .to[automata.storage[idx].par_symb_idx];
                if (parent_link == 0 &&
                    automata.storage[idx].suffix_link == INT64_MAX) {
                    automata.storage[idx].suffix_link = 0;
                    break;
                }
                parent_link = automata.storage[parent_link].suffix_link;
            }
            if (automata.storage[automata.storage[idx].
                    suffix_link].str_num_if_term) {
                automata.storage[idx].output_link =
                        automata.storage[idx].suffix_link;
            } else {
                automata.storage[idx].output_link =
                        automata.storage[automata.storage[idx].
                                suffix_link].output_link;
            }
            for (long long i = 0; i < ALPH_SIZE; ++i) {
                if (automata.storage[idx].to[i] != INT64_MAX)
                    order.push(automata.storage[idx].to[i]);
            }
        }
        return automata;
    }


    static std::vector<std::vector<long long>> find_all_entries(trie &automata,
                                                                const std::string &text) {
        std::vector<std::vector<long long>> res(automata.max_str_num);

        long long curr_state = 0, copy;
        for (long long i = 0; i < text.size(); ++i) {
            curr_state = automata.calc_jump(curr_state, get_idx(text[i]));
            copy = curr_state;

            while (copy) {
                if (automata.storage[copy].str_num_if_term) {
                    res[automata.storage[copy].str_num_if_term - 1].push_back(i + 2);
                }
                copy = automata.storage[copy].output_link;
            }
        }
        return res;
    }

    long long solve(long long n) {
        for (long long i = 0; i < storage.size(); ++i) {
            if (storage[i].str_num_if_term || storage[i].output_link != 0) {
                storage[i].term_val = 1;
            } else {
                storage[i].nonterm_val = 1;
            }
        }
        for (long long k = 0; k < n; ++k) {
            for (long long i = 0; i < storage.size(); ++i) {
                storage[i].tm_copy = storage[i].term_val;
                storage[i].ntm_copy = storage[i].nonterm_val;
                storage[i].term_val = 0;
                storage[i].nonterm_val = 0;
            }
            for (long long i = 0; i < storage.size(); ++i) {
                for (long long s = 0; s < ALPH_SIZE; ++s) {
                    long long vt = calc_jump(i, s);
                    if (storage[i].str_num_if_term || storage[i].output_link != 0) {
                        storage[i].term_val = (storage[i].term_val +
                                               storage[vt].tm_copy +
                                               storage[vt].ntm_copy) % 10000;
                    } else {
                        storage[i].nonterm_val = (storage[i].nonterm_val +
                                                  storage[vt].ntm_copy) % 10000;
                        storage[i].term_val = (storage[i].term_val +
                                               storage[vt].tm_copy) % 10000;
                    }
                }
            }
        }
        return storage[0].term_val;
    }
};


//////////////////////////////



int32_t main() {
    std::ios::sync_with_stdio(0);
    std::cin.tie(0);

    std::vector<std::string> dict;

    long long num, cnt;

    std::cin >> cnt >> num;
    dict.resize(num);

    for (auto &pattern : dict) {
        std::cin >> pattern;
    }

    auto automata = trie::aho_corasick(dict);
    std::cout << automata.solve(cnt);
    return 0;
}