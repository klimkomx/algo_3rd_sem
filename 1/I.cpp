#include <iostream>
#include <vector>
#include <string>
#include <algorithm>


std::vector<size_t> z_function(const std::string& str) {
    std::vector<size_t> z_f(str.size(), 0);
    size_t lb = 0, rb = 0;

    for (size_t i = 1; i < z_f.size(); ++i) {
        if (i < rb)
	    z_f[i] = std::min(z_f[i - lb], rb - i);
	while (i + z_f[i] <= str.size() && str[z_f[i]] == str[z_f[i] + i])
	    ++z_f[i];
	if (i + z_f[i] > rb)
	    rb = i + z_f[i], 
	    lb = i;
    }
    return z_f; 
}


int32_t main() {
    std::string s;
    std::cin >> s;

    for (const size_t& pos : z_function(s))
        std::cout << pos << " ";
    std::cout << "\n";
    return 0;
}
