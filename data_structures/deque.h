#ifndef DEQUE_H
#define DEQUE_H

#include<vector>
#include<stdexcept>
#include <iostream>
#include<iterator>
using std::vector;

template<typename T>
class Deque {
private:
    static const size_t BUCKET_SIZE = 64;
    vector<T*> base;
    size_t ft = 0, sd = 0, length = 0,
            bucketlist_begin, bucketlist_end, bucketlist_size,
            fromstartpoint;
    void cancel_resizing(bool onleft = false);
    void resize_bucket_list(bool onleft = false);
public:
    friend class iteratorTemp;

    //iteratorTemps
    template<typename ref, typename point>
class iteratorTemp {
    protected:
        const Deque<T>* obj;
        size_t ft, sd, frompoint;
    public:
        friend class Deque<T>;

        iteratorTemp() = delete;
        iteratorTemp(const Deque<T>& obj, size_t ft, size_t sd, size_t pointfrom);
        iteratorTemp(const Deque<T>::iteratorTemp<ref, point>& other) = default;
        //increment/decrement
        iteratorTemp& operator++();
        iteratorTemp& operator--();
        iteratorTemp operator++(int);
        iteratorTemp operator--(int);
        iteratorTemp& operator+=(long long);
        iteratorTemp& operator-=(long long);
        iteratorTemp operator+(long long) const;
        iteratorTemp operator-(long long) const;

        bool operator<(const iteratorTemp& value) const;
        bool operator>(const iteratorTemp& value) const;
        bool operator<=(const iteratorTemp& value) const;
        bool operator>=(const iteratorTemp& value) const;
        bool operator==(const iteratorTemp& value) const;
        bool operator!=(const iteratorTemp& value) const;

        long long operator-(const iteratorTemp& value) const;

        ref operator*() const;
        point operator->() const;

        operator iteratorTemp<const ref, const point>() const;
        using iterator_category = std::random_access_iterator_tag;
        using value_type = T;
        using difference_type = long long;
        using reference = T&;
        using pointer = T*;
    };

    typedef iteratorTemp<T&, T*> iterator;
    typedef iteratorTemp<const T&, const T*> const_iterator;
    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

    Deque();
    Deque(const Deque<T>& other);
    Deque(size_t d_length, const T& elem = T());
    ~Deque();

    size_t size() const;
    Deque<T>& operator=(const Deque<T>& other);
    T& operator[](size_t index);
    const T& operator[](size_t index) const;
    T& at(size_t index);
    const T& at(const size_t index) const;
    void push_back(const T& val);
    void push_front(const T& val);
    void pop_back();
    void pop_front();

    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
    const_iterator cbegin() const;
    const_iterator cend() const;

    reverse_iterator rbegin();
    reverse_iterator rend();
    const_reverse_iterator rbegin() const;
    const_reverse_iterator rend() const;
    const_reverse_iterator crbegin() const;
    const_reverse_iterator crend() const;

    void insert(iterator, const T& val);
    void erase(iterator);
};

template<typename T>
void Deque<T>::cancel_resizing(bool onleft) {
    bucketlist_size >>= 1ull;
    base.resize(bucketlist_size);
    if (onleft) {
        bucketlist_begin -= bucketlist_size;
        bucketlist_end -= bucketlist_size;
    }
}

template<typename T>
void Deque<T>::resize_bucket_list(bool onleft) {
    base.resize(2ull * bucketlist_size);
    if (onleft) {
        for (size_t i = 0; i < std::min(bucketlist_size, bucketlist_end); ++i) {
            base[i + bucketlist_size] = base[i];
        }
        bucketlist_begin = bucketlist_size;
        fromstartpoint += bucketlist_size;
        bucketlist_end += bucketlist_size;

    }
    bucketlist_size += bucketlist_size;
}

template<typename T>
Deque<T>::Deque() {
    ft = sd = 0ull;
    fromstartpoint = bucketlist_begin = 0ull;
    bucketlist_end = length = bucketlist_size = 1ull;
    base.resize(1, reinterpret_cast<T*>(new int8_t[BUCKET_SIZE * sizeof(T)]));
}

template<typename T>
Deque<T>::Deque(const Deque<T>& other) {
    base.resize(other.bucketlist_size, nullptr);
    ft = other.ft;
    sd = other.sd;
    bucketlist_begin = other.bucketlist_begin;
    bucketlist_end = other.bucketlist_end;
    bucketlist_size = other.bucketlist_size;
    fromstartpoint = other.fromstartpoint;
    length = other.length;

    for (size_t i = bucketlist_begin; i < bucketlist_end; ++i)
        base[i] = reinterpret_cast<T*>(new int8_t[BUCKET_SIZE * sizeof(T)]);
    size_t ft_it = 0, sd_it = ft;
    try {
        for (; ft_it + 1ull< length || sd_it < sd;) {
            new(base[bucketlist_begin + ft_it] + sd_it) T(other.base[bucketlist_begin + ft_it][sd_it]);
            ++sd_it;
            if (sd_it == BUCKET_SIZE) {
                sd_it = 0ull;
                ++ft_it;
            }
        }
    } catch(...) {
        size_t ft_it_c = 0, sd_it_c = ft;
        while (ft_it_c < ft_it || sd_it_c <= sd_it) {
            (base[bucketlist_begin + ft_it_c] + sd_it_c) -> ~T();

            ++sd_it_c;
            if (sd_it_c == BUCKET_SIZE) {
                sd_it_c = 0ull;
                ++ft_it_c;
            }
        }
        for (size_t i = bucketlist_begin; i < bucketlist_end; ++i) {
            delete[] reinterpret_cast<int8_t*>(base[i]);
        }
        fromstartpoint = 0ull;
        ft = sd = bucketlist_begin = bucketlist_end = bucketlist_size = length = 0ull;
        base.resize(0);
        throw;
    }
}

template<typename T>
Deque<T>::Deque(size_t d_length, const T& elem) {
    bucketlist_end = bucketlist_size = length = d_length / BUCKET_SIZE + 1ull;
    fromstartpoint = bucketlist_begin = 0ull;
    ft = 0ull;
    sd = d_length % BUCKET_SIZE;
    base.resize(length);
    for (size_t i = 0; i < length; ++i)
        base[i] = reinterpret_cast<T*>(new int8_t[BUCKET_SIZE * sizeof(T)]);
    size_t ft_it = 0, sd_it = ft;
    try {
        for (; ft_it + 1ull < length || sd_it < sd;) {
            new(base[ft_it] + sd_it) T(elem);
            ++sd_it;
            if (sd_it == BUCKET_SIZE) {
                sd_it = 0ull;
                ++ft_it;
            }
        }
    } catch(...) {
        size_t ft_it_c = 0, sd_it_c = ft;
        while (ft_it_c < ft_it || sd_it_c <= sd_it) {
            (base[ft_it_c] + sd_it_c) -> ~T();

            ++sd_it_c;
            if (sd_it_c == BUCKET_SIZE) {
                sd_it_c = 0ull;
                ++ft_it_c;
            }
        }
        for (size_t i = 0; i < length; ++i) {
            delete[] reinterpret_cast<int8_t*>(base[i]);
        }
        fromstartpoint = ft = sd = bucketlist_begin = bucketlist_end = bucketlist_size = length = 0ull;
        base.resize(0);
        throw;
    }
}

template<typename T>
Deque<T>::~Deque() {
    for (size_t ft_it = 0, sd_it = ft; ft_it + 1ull < length  || sd_it < sd;) {
        (base[bucketlist_begin + ft_it] + sd_it) -> ~T();

        ++sd_it;
        if (sd_it == BUCKET_SIZE) {
            sd_it = 0ull;
            ++ft_it;
        }
    }
    for (size_t i = 0; i < length; ++i) {
        delete[] reinterpret_cast<int8_t*>(base[bucketlist_begin + i]);
    }
}

template<typename T>
size_t Deque<T>::size() const{
    return (BUCKET_SIZE * length) - ft - (BUCKET_SIZE - sd);
}

template<typename T>
T& Deque<T>::operator[](size_t index) {
    return base[bucketlist_begin + (index + ft) / BUCKET_SIZE][(index + ft) % BUCKET_SIZE];
}

template<typename T>
const T& Deque<T>::operator[](size_t index) const {
    return base[bucketlist_begin + (index + ft) / BUCKET_SIZE][(index + ft) % BUCKET_SIZE];
}

template<typename T>
T& Deque<T>::at(size_t index) {
    if ((index + ft) / BUCKET_SIZE >= length ||
        ((index + ft) / BUCKET_SIZE + 1ull == length && ((index + ft) % BUCKET_SIZE) >= sd)) {
        throw std::out_of_range("");
    }
    return base[bucketlist_begin + (index + ft) / BUCKET_SIZE][(index + ft) % BUCKET_SIZE];
}

template<typename T>
const T& Deque<T>::at(size_t index) const {
    if ((index + ft) / BUCKET_SIZE >= length ||
        ((index + ft) / BUCKET_SIZE +1ull == length && (index + ft) % BUCKET_SIZE >= sd))
        throw std::out_of_range("");
    return base[bucketlist_begin + (index + ft) / BUCKET_SIZE][(index + ft) % BUCKET_SIZE];
}

template<typename T>
void Deque<T>::push_back(const T& val) {
    T* newbucket = nullptr;
    if (BUCKET_SIZE == sd + 1ull)
        newbucket = reinterpret_cast<T*>(new int8_t[BUCKET_SIZE * sizeof(T)]);
    try {
        new(base[bucketlist_end - 1ull] + sd) T(val);
        ++sd;
        if (sd == BUCKET_SIZE) {
            ++length;
            sd = 0;
            if (bucketlist_end == bucketlist_size) {
                resize_bucket_list();
            }
            base[bucketlist_end++] = newbucket;
        }
    } catch(...) {
        if (sd + 1ull == BUCKET_SIZE) {
            delete[] reinterpret_cast<int8_t*>(newbucket);
        }
        throw;
    }
}

template<typename T>
void Deque<T>::push_front(const T& val) {
    T* newbucket = nullptr;
    if (ft == 0ull)
        newbucket = reinterpret_cast<T*>(new int8_t[BUCKET_SIZE * sizeof(T)]);

    try {
        if (ft == 0ull) {
            ft = BUCKET_SIZE - 1ull;
            new(newbucket + ft) T(val);
            if (bucketlist_begin == 0ull) {
                resize_bucket_list(true);
            }
            base[--bucketlist_begin] = newbucket;
            ++length;

        } else {
            --ft;
            new(base[bucketlist_begin] + ft) T(val);
        }

    } catch(...) {
        if (ft == BUCKET_SIZE - 1ull) {
            ft = 0ull;
            delete[] reinterpret_cast<int8_t*>(newbucket);
        } else {
            ++ft;
        }
        throw;
    }
}

template<typename T>
void Deque<T>::pop_back() {
    if (sd == 0ull) {
        delete[] reinterpret_cast<int8_t*>(base[bucketlist_end - 1ull]);
        sd = BUCKET_SIZE - 1ull;
        --bucketlist_end;
        --length;
    } else
        --sd;

    base[bucketlist_end - 1ull][sd].~T();
}

template<typename T>
void Deque<T>::pop_front() {
    base[bucketlist_begin][ft].~T();
    if (ft == BUCKET_SIZE - 1ull) {
        delete[] reinterpret_cast<int8_t*>(base[bucketlist_begin]);
        ft = 0ull;
        ++bucketlist_begin;
        --length;
    } else
        ++ft;
}

template<typename T>
Deque<T>& Deque<T>::operator=(const Deque<T> &other) {
    std::vector<T*> nbase(other.bucketlist_size, nullptr);

    size_t nft = other.ft;
    size_t nsd = other.sd;
    size_t nfromstartpoint = other.fromstartpoint;
    size_t nbucketlist_begin = other.bucketlist_begin;
    size_t nbucketlist_end = other.bucketlist_end;
    size_t nbucketlist_size = other.bucketlist_size;
    size_t nlength = other.length;

    for (size_t i = nbucketlist_begin; i < nbucketlist_end; ++i)
        nbase[i] = reinterpret_cast<T*>(new int8_t[BUCKET_SIZE * sizeof(T)]);

    size_t ft_it = 0, sd_it = nft;
    try {
        for (; ft_it + 1ull < nlength || sd_it < nsd;) {
            new(nbase[nbucketlist_begin + ft_it] + sd_it) T(other.base[nbucketlist_begin + ft_it][sd_it]);
            ++sd_it;
            if (sd_it == BUCKET_SIZE) {
                sd_it = 0ull;
                ++ft_it;
            }
        }
    } catch(...) {
        size_t ft_it_c = 0, sd_it_c = nft;
        while (ft_it_c < ft_it || sd_it_c <= sd_it) {
            (nbase[nbucketlist_begin + ft_it_c] + sd_it_c) -> ~T();

            ++sd_it_c;
            if (sd_it_c == BUCKET_SIZE) {
                sd_it_c = 0ull;
                ++ft_it_c;
            }
        }
        for (size_t i = nbucketlist_begin; i < nbucketlist_end; ++i) {
            delete[] reinterpret_cast<int8_t*>(nbase[i]);
        }
        nfromstartpoint = nft = nsd = nbucketlist_begin = nbucketlist_end = nbucketlist_size = nlength = 0ull;
        nbase.resize(0);
        throw;
    }


    for (size_t ft_it = 0, sd_it = ft; ft_it + 1ull < length || sd_it < sd;) {
        (base[bucketlist_begin + ft_it] + sd_it) -> ~T();

        ++sd_it;
        if (sd_it == BUCKET_SIZE) {
            sd_it = 0ull;
            ++ft_it;
        }
    }
    for (size_t i = 0; i < length; ++i) {
        delete[] reinterpret_cast<int8_t*>(base[bucketlist_begin + i]);
    }

    base = nbase;
    length = nlength;
    fromstartpoint = nfromstartpoint;
    bucketlist_end = nbucketlist_end;
    bucketlist_size = nbucketlist_size;
    bucketlist_begin = nbucketlist_begin;
    ft = nft;
    sd = nsd;
    return *this;
}

template<typename T>
typename Deque<T>::iterator Deque<T>::begin() {
    return iterator(*this, bucketlist_begin, ft, fromstartpoint);
}

template<typename T>
typename Deque<T>::iterator Deque<T>::end() {
    return iterator(*this, bucketlist_end - 1ull, sd, fromstartpoint);
}

template<typename T>
typename Deque<T>::const_iterator Deque<T>::begin() const {
    return const_iterator(*this, bucketlist_begin, ft, fromstartpoint);
}

template<typename T>
typename Deque<T>::const_iterator Deque<T>::end() const {
    return const_iterator(*this, bucketlist_end - 1ull, sd, fromstartpoint);
}

template<typename T>
typename Deque<T>::const_iterator Deque<T>::cbegin() const {
    return const_iterator(*this, bucketlist_begin, ft, fromstartpoint);
}

template<typename T>
typename Deque<T>::const_iterator Deque<T>::cend() const {
    return const_iterator(*this, bucketlist_end - 1ull, sd, fromstartpoint);
}

template<typename T>
typename Deque<T>::reverse_iterator Deque<T>::rbegin() {
    return reverse_iterator(end());
}

template<typename T>
typename Deque<T>::reverse_iterator Deque<T>::rend() {
    return reverse_iterator(begin());
}

template<typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::rbegin() const {
    return const_reverse_iterator(end());
}

template<typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::rend() const {
    return const_reverse_iterator(begin());
}

template<typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::crbegin() const {
    return const_reverse_iterator(cend());
}

template<typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::crend() const {
    const_reverse_iterator(cbegin() + 0ull);
}

template<typename T>
void Deque<T>::insert(Deque<T>::iterator it, const T &val) {
    try {
        push_back(val);
        for (iterator a = --end(); a > it; --a) {
            std::swap(*a, *(a - 1));
        }
    } catch (...) {
        throw;
    }
}

template<typename T>
void Deque<T>::erase(Deque<T>::iterator it) {
    for (; it < end(); ++it)
        std::swap(*it, *(it + 1));
    pop_back();
}


template<typename T>
template<typename ref, typename point>
Deque<T>::iteratorTemp<ref, point>::iteratorTemp(const Deque<T>& obj_, size_t ft_, size_t sd_, size_t pointfrom):
        obj(&obj_), ft(ft_), sd(sd_), frompoint(pointfrom){}

template<typename T>
template<typename ref, typename point>
typename Deque<T>::template iteratorTemp<ref, point>& Deque<T>::iteratorTemp<ref, point>::operator+=(long long plus) {
    if (plus < 0ll) {
        return operator-=(-plus);
    }
    sd += static_cast<size_t>(plus);
    ft += sd / Deque<T>::BUCKET_SIZE;
    sd %= Deque<T>::BUCKET_SIZE;

    return *this;
}

template<typename T>
template<typename ref, typename point>
typename Deque<T>::template iteratorTemp<ref, point>& Deque<T>::iteratorTemp<ref, point>::operator-=(long long plus) {
    if (plus < 0ll) {
        return operator+=(-plus);
    }
    size_t minus = static_cast<size_t>(plus);
    ft -= minus/Deque<T>::BUCKET_SIZE;
    minus %= Deque<T>::BUCKET_SIZE;
    if (sd < minus) {
        sd += Deque<T>::BUCKET_SIZE;
        --ft;
    }
    sd -= minus;
    return *this;
}

template<typename T>
template<typename ref, typename point>
typename Deque<T>::template iteratorTemp<ref, point>& Deque<T>::iteratorTemp<ref, point>::operator++() {
    return operator+=(1ll);
}

template<typename T>
template<typename ref, typename point>
typename Deque<T>::template iteratorTemp<ref, point>& Deque<T>::iteratorTemp<ref, point>::operator--() {
    return operator-=(1ll);
}

template<typename T>
template<typename ref, typename point>
typename Deque<T>::template iteratorTemp<ref, point> Deque<T>::iteratorTemp<ref, point>::operator++(int) {
    Deque<T>::iteratorTemp<ref, point> it(*this);
    this -> operator+=(1ll);
    return it;
}

template<typename T>
template<typename ref, typename point>
typename Deque<T>::template iteratorTemp<ref, point> Deque<T>::iteratorTemp<ref, point>::operator--(int) {
    Deque<T>::iteratorTemp<ref, point> it(*this);
    this -> operator-=(1ll);
    return it;
}

template<typename T>
template<typename ref, typename point>
typename Deque<T>::template iteratorTemp<ref, point> Deque<T>::iteratorTemp<ref, point>::operator+(long long plus) const {
    Deque<T>::iteratorTemp<ref, point> it(*this);
    return it.operator+=(plus);
}

template<typename T>
template<typename ref, typename point>
typename Deque<T>::template iteratorTemp<ref, point> Deque<T>::iteratorTemp<ref, point>::operator-(long long plus) const {
    Deque<T>::iteratorTemp<ref, point> it(*this);
    return it.operator-=(plus);
}

template<typename T>
template<typename ref, typename point>
bool Deque<T>::iteratorTemp<ref, point>::operator<(const Deque<T>::iteratorTemp<ref, point>& value) const{
    return (ft + (obj->fromstartpoint - frompoint) < value.ft + (value.obj -> fromstartpoint - value.frompoint)
            || (ft + (obj->fromstartpoint - frompoint) == value.ft + (value.obj -> fromstartpoint - value.frompoint)
             && sd < value.sd));
}

template<typename T>
template<typename ref, typename point>
bool Deque<T>::iteratorTemp<ref, point>::operator>(const Deque<T>::iteratorTemp<ref, point>& value) const{
    return ((ft + (obj->fromstartpoint - frompoint) > value.ft + (value.obj -> fromstartpoint - value.frompoint))
            || (ft + (obj->fromstartpoint - frompoint) == value.ft + (value.obj -> fromstartpoint - value.frompoint)
            && sd > value.sd));
}

template<typename T>
template<typename ref, typename point>
bool Deque<T>::iteratorTemp<ref, point>::operator==(const Deque<T>::iteratorTemp<ref, point>& value) const{
    return (ft + (obj->fromstartpoint - frompoint) == value.ft + (value.obj -> fromstartpoint - value.frompoint)
            && sd == value.sd);
}

template<typename T>
template<typename ref, typename point>
bool Deque<T>::iteratorTemp<ref, point>::operator!=(const Deque<T>::iteratorTemp<ref, point>& value) const{
    return !(this->operator==(value));
}

template<typename T>
template<typename ref, typename point>
bool Deque<T>::iteratorTemp<ref, point>::operator<=(const Deque<T>::iteratorTemp<ref, point>& value) const{
    return !(this->operator>(value));
}

template<typename T>
template<typename ref, typename point>
bool Deque<T>::iteratorTemp<ref, point>::operator>=(const Deque<T>::iteratorTemp<ref, point>& value) const{
    return !(this->operator<(value));
}

template<typename T>
template<typename ref, typename point>
long long Deque<T>::iteratorTemp<ref, point>::operator-(const Deque<T>::iteratorTemp<ref, point>& value) const{
    return static_cast<long long>(Deque<T>::BUCKET_SIZE) *
          (static_cast<long long>(ft + obj -> fromstartpoint - frompoint) -
           static_cast<long long>(value.ft + obj -> fromstartpoint - value.frompoint)) +
           static_cast<long long>(sd) - static_cast<long long>(value.sd);
}

template<typename T>
template<typename ref, typename point>
ref Deque<T>::iteratorTemp<ref, point>::operator*() const{
    return obj->base[ft + obj -> fromstartpoint - frompoint][sd];
}

template<typename T>
template<typename ref, typename point>
point Deque<T>::iteratorTemp<ref, point>::operator->() const{
    return &(obj->base[ft + obj -> fromstartpoint - frompoint][sd]);
}

template<typename T>
template<typename ref, typename point>
Deque<T>::iteratorTemp<ref, point>::operator iteratorTemp<const ref, const point>() const {
    return iteratorTemp<const ref, const point>(obj, ft, sd, frompoint);
}

#endif