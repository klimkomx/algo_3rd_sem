#pragma once
#include <vector>
#include <type_traits>
#include <memory>

using std::shared_ptr;

//FIXED
template<size_t chunkSize>
class FixedAllocator {
private:
    const size_t BIG_CHUNK_SIZE = 500;
    static shared_ptr<FixedAllocator<chunkSize> > instance;
    size_t it = 0;
    std::vector<char*> chunks;
    FixedAllocator() = default;
    void makeLongChunk();

public:
    FixedAllocator(FixedAllocator const &) = delete;
    FixedAllocator& operator=(FixedAllocator const&) = delete;

    static shared_ptr<FixedAllocator<chunkSize>>& getInstance();
    char* makeChunk();
    void deleteChunk(char* ptr, size_t n);
    ~FixedAllocator();
};


template<size_t chunkSize>
shared_ptr<FixedAllocator<chunkSize> > FixedAllocator<chunkSize>::instance = nullptr;

template<size_t chunkSize>
FixedAllocator<chunkSize>::~FixedAllocator() {
    for(size_t i = 0; i < chunks.size(); ++i) {
        delete [] chunks[i];
    }
}

template<size_t chunkSize>
shared_ptr<FixedAllocator<chunkSize>>& FixedAllocator<chunkSize>::getInstance() {
    if (instance == nullptr)
        instance = shared_ptr<FixedAllocator<chunkSize>>(new FixedAllocator<chunkSize>());
    return instance;
}

template<size_t chunkSize>
void FixedAllocator<chunkSize>::makeLongChunk() {
    chunks.push_back(new char [BIG_CHUNK_SIZE * chunkSize]);
    it = 0;
}

template<size_t chunkSize>
char* FixedAllocator<chunkSize>::makeChunk() {
    if (it == BIG_CHUNK_SIZE - 1) {
        makeLongChunk();
    }
    return chunks.back() + it * chunkSize;
}

template<size_t chunkSize>
void FixedAllocator<chunkSize>::deleteChunk(char* ptr, size_t n) {
}

//FAST
template<typename T>
class FastAllocator {
private:
    static const size_t maxSize = 50;
public:
    FastAllocator() = default;
    FastAllocator(const FastAllocator& allocator) = default;
    ~FastAllocator() = default;
    static T* allocate(size_t n);
    static void deallocate(T* ptr, size_t n);

    using value_type = T;
    template<typename U>
    struct rebind {
        typedef FastAllocator<U> other;
    };
};


template<typename T>
T* FastAllocator<T>::allocate(size_t n) {
    if (n * sizeof(T) <= maxSize) {
        return FixedAllocator<n * sizeof(T)>::getInstance() -> makeChunk();
    } else {
        return new T[n];
    }
}

template<typename T>
void FastAllocator<T>::deallocate(T *ptr, size_t n) {
    if (n * sizeof(T) <= maxSize){
        FixedAllocator<n * sizeof(T)>::getInstance() -> deleteChunk();
    } else {
        delete[] ptr;
    }
}


//LIST

template<typename T, typename Allocator = std::allocator<T>>
class List {
private:
      struct NodeBase {
          NodeBase* prev = nullptr,
                    next = nullptr;
          NodeBase() = default;
          NodeBase(const NodeBase&) = default;
          ~NodeBase() = default;
      };
     struct Node : NodeBase {
         T value;
         explicit Node(const T& val = T()): NodeBase(), value(val){};
         ~Node() = default;
     };
     Allocator allocator_;
     size_t size_;
    NodeBase* list;

    using AllocatorTraits = typename std::allocator_traits<Allocator>;
    using AllocatorTraitsRebinded = typename AllocatorTraits:: template rebind<Node>::other;

    Node* makeNewNode(const T& elem);
    void eraseNode(Node*);
    void connect(NodeBase*, NodeBase*);
public:
    explicit List(const Allocator& alloc = Allocator());
    List(size_t count, const T& value = T(), const Allocator& alloc = Allocator());
    List(const List& other);
    List<T, Allocator>& operator=(const List& other);

    Allocator& get_allocator();
    size_t size();
    void push_back(const T& elem);
    void push_front(const T& elem);
    void pop_back();
    void pop_front();

    friend class iteratorTemp;

    template<typename ref, typename point>
    class iteratorTemp {
    protected:
        NodeBase* obj;
    public:
        friend class List<T, Allocator>;

        iteratorTemp() = delete;
        iteratorTemp(const List<T, Allocator>::Node* node): obj(node){};
        iteratorTemp(const List<T, Allocator>::iteratorTemp<ref, point>& other) = default;
        ~iteratorTemp() = default;
        //increment/decrement
        iteratorTemp& operator++();
        iteratorTemp& operator--();
        iteratorTemp operator++(int);
        iteratorTemp operator--(int);
        bool operator==(const iteratorTemp& value) const;
        bool operator!=(const iteratorTemp& value) const;

        ref operator*() const;
        point operator->() const;

        operator iteratorTemp<const ref, const point>() const;
        using iterator_category = std::random_access_iterator_tag;
        using value_type = T;
        using difference_type = long long;
        using reference = T&;
        using pointer = T*;
    };

    typedef iteratorTemp<T&, T*> iterator;
    typedef iteratorTemp<const T&, const T*> const_iterator;
    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
    const_iterator cbegin() const;
    const_iterator cend() const;

    reverse_iterator rbegin();
    reverse_iterator rend();
    const_reverse_iterator rbegin() const;
    const_reverse_iterator rend() const;
    const_reverse_iterator crbegin() const;
    const_reverse_iterator crend() const;

    void insert(const iterator&, const T&);
    void erase(const iterator&);
};

template<typename T, typename Allocator>
template<typename ref, typename point>
typename List<T, Allocator>::template iteratorTemp<ref, point> &List<T, Allocator>::iteratorTemp<ref, point>::operator++() {
    return obj = obj ->next;
}

template<typename T, typename Allocator>
template<typename ref, typename point>
typename List<T, Allocator>::template iteratorTemp<ref, point> &List<T, Allocator>::iteratorTemp<ref, point>::operator--() {
    return obj = obj ->prev;
}

template<typename T, typename Allocator>
template<typename ref, typename point>
typename List<T, Allocator>::template iteratorTemp<ref, point> List<T, Allocator>::iteratorTemp<ref, point>::operator++(int) {
    auto tmp = obj;
    obj = obj ->next;
    return tmp;
}

template<typename T, typename Allocator>
template<typename ref, typename point>
typename List<T, Allocator>::template iteratorTemp<ref, point> List<T, Allocator>::iteratorTemp<ref, point>::operator--(int) {
    auto tmp = obj;
    obj = obj ->prev;
    return tmp;
}

template<typename T, typename Allocator>
template<typename ref, typename point>
bool List<T, Allocator>::template iteratorTemp<ref, point>::operator==(const List::iteratorTemp<ref, point> &value) const {
    return obj == value.obj;
}

template<typename T, typename Allocator>
template<typename ref, typename point>
bool List<T, Allocator>::template iteratorTemp<ref, point>::operator!=(const List::iteratorTemp<ref, point> &value) const {
    return obj != value.obj;
}

template<typename T, typename Allocator>
template<typename ref, typename point>
ref List<T, Allocator>::iteratorTemp<ref, point>::operator*() const {
    return reinterpret_cast<Node*>(obj) ->value;
}

template<typename T, typename Allocator>
template<typename ref, typename point>
point List<T, Allocator>::iteratorTemp<ref, point>::operator->() const {
    return &reinterpret_cast<Node*>(obj) ->value;
}

template<typename T, typename Allocator>
template<typename ref, typename point>
List<T, Allocator>::iteratorTemp<ref, point>::operator iteratorTemp<const ref, const point>() const {
    return List::iteratorTemp<const ref, const point>(obj);
}













template<typename T, typename Allocator>
List<T, Allocator>::List(const Allocator &alloc):allocator_(alloc), size_(0), list(new NodeBase()) {
    connect(list, list);
}

template<typename T, typename Allocator>
List<T, Allocator>::List(size_t count, const T &value, const Allocator &alloc):list(alloc) {
    while (count--) {
        push_back(value);
    }
}

template<typename T, typename Allocator>
List<T, Allocator>::List(const List &other) {
    allocator_ = AllocatorTraits::select_on_container_copy_construction(other.allocator_);
    auto it = other.begin();
    while (it != other.end()) {
        push_back(*it);
        ++it;
    }
}

template<typename T, typename Allocator>
List<T, Allocator> &List<T, Allocator>::operator=(const List &other) {
    while (size_)
        pop_back();
    allocator_ = AllocatorTraits::propagate_on_container_copy_assignment(other.allocator_);
    auto it = other.begin();
    while (it != other.end()) {
        push_back(*it);
        ++it;
    }
}

template<typename T, typename Allocator>
Allocator &List<T, Allocator>::get_allocator() {
    return allocator_;
}

template<typename T, typename Allocator>
size_t List<T, Allocator>::size() {
    return size_;
}

template<typename T, typename Allocator>
void List<T, Allocator>::push_back(const T &elem) {
    Node* newNode = makeNewNode(elem);
    connect(list->prev, newNode);
    connect(newNode, list);
}

template<typename T, typename Allocator>
void List<T, Allocator>::push_front(const T &elem) {
    Node* newNode = makeNewNode(elem);
    connect(newNode, list->next);
    connect(list, newNode);
}

template<typename T, typename Allocator>
void List<T, Allocator>::pop_back() {
    Node* copy = list -> prev;
    connect(copy -> prev, list);
    eraseNode(copy);
}

template<typename T, typename Allocator>
void List<T, Allocator>::pop_front() {
    Node* copy = list -> next;
    connect(list, copy -> next);
    eraseNode(copy);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::Node *List<T, Allocator>::makeNewNode(const T& elem) {
    ++size_;
    Node* newNode = AllocatorTraitsRebinded::allocate(allocator_, 1);
    AllocatorTraitsRebinded::construct(allocator_, newNode, elem);
    return newNode;
}

template<typename T, typename Allocator>
void List<T, Allocator>::eraseNode(typename List<T, Allocator>::Node * node) {
    --size_;
    AllocatorTraitsRebinded::deconstruct(allocator_, node);
    AllocatorTraitsRebinded::deallocate(allocator_, 1);
}

template<typename T, typename Allocator>
void List<T, Allocator>::connect(NodeBase * ft, NodeBase * sd) {
    ft -> next = sd;
    sd ->prev = ft;
}


template<typename T, typename Allocator>
typename List<T, Allocator>::iterator List<T, Allocator>::begin() {
    return List::iterator(list -> next);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::iterator List<T, Allocator>::end() {
    return List::iterator(list);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::begin() const {
    return List::const_iterator(list -> next);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::end() const {
    return List::const_iterator(list);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::cbegin() const {
    return List::const_iterator(list -> next);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::cend() const {
    return List::const_iterator(list);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::reverse_iterator List<T, Allocator>::rbegin() {
    return List::reverse_iterator(list -> next);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::reverse_iterator List<T, Allocator>::rend() {
    return List::reverse_iterator(list);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::rbegin() const {
    return List::const_reverse_iterator(list -> next);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::rend() const {
    return List::const_reverse_iterator(list);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::crbegin() const {
    return List::const_reverse_iterator(list -> next);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::crend() const {
    return List::const_reverse_iterator(list);
}

template<typename T, typename Allocator>
void List<T, Allocator>::insert(const List::iterator& it, const T &val) {
    Node* node = makeNewNode(val);
    connect((*it) -> prev, node);
    connect(node, *it);
}

template<typename T, typename Allocator>
void List<T, Allocator>::erase(const List::iterator& it) {
    connect((*it) -> prev, (*it) -> next);
    eraseNode(*it);
}