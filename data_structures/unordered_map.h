#include <vector>
#include <type_traits>
#include <memory>
#include <iostream>
#include <queue>

//LIST

template<typename T, typename Allocator = std::allocator<T>>
class List {
private:
    struct NodeBase {
        NodeBase* prev = nullptr;
        NodeBase* next = nullptr;
        NodeBase() = default;
        NodeBase(const NodeBase&) = default;
        ~NodeBase() = default;
    };
    struct Node : NodeBase {
        T value;
        explicit Node(const T& val): NodeBase(), value(val){};
        explicit Node(T&& val): NodeBase(), value(std::move(val)){};
        explicit Node(): NodeBase(), value(){};
        ~Node() = default;
    };
    using AllocatorTraits = typename std::allocator_traits<Allocator>::template rebind_traits<Node>;
    using AllocatorType = typename std::allocator_traits<Allocator>::template rebind_alloc<Node>;
    AllocatorType allocator_;
    Allocator allocator_m;
    size_t size_;
    NodeBase* list;


    Node* makeNewNode(const T& elem);
    void eraseNode(Node*);
    void connect(NodeBase*, NodeBase*);
public:
    explicit List(const Allocator& alloc = Allocator());
    List(size_t count, const Allocator& alloc = Allocator());
    List(size_t count, const T& value, const Allocator& alloc = Allocator());
    List(const List& other);
    List(List&& other) noexcept;
    List<T, Allocator>& operator=(const List& other);
    List<T, Allocator>& operator=(List&& other) noexcept;
    ~List();

    Allocator get_allocator();
    size_t size() const;
    void push_back(const T& elem);
    void push_front(const T& elem);
    void pop_back();
    void pop_front();

    friend class iteratorTemp;

    template<typename vtype>
    class iteratorTemp {
    protected:
        NodeBase* obj;
    public:
        friend class List<T, Allocator>;

        iteratorTemp() = delete;
        explicit iteratorTemp(List<T, Allocator>::NodeBase* const node): obj(node){};
        iteratorTemp(const List<T, Allocator>::iteratorTemp<vtype>& other) = default;
        ~iteratorTemp() = default;
        //increment/decrement
        iteratorTemp& operator++();
        iteratorTemp& operator--();
        iteratorTemp operator++(int);
        iteratorTemp operator--(int);
        bool operator==(const iteratorTemp& value) const;
        bool operator!=(const iteratorTemp& value) const;

        vtype& operator*() const;
        vtype* operator->() const;

        using iterator_category = std::bidirectional_iterator_tag;
        using value_type = T;
        using difference_type = long long;
        using reference = vtype&;
        using pointer = vtype*;

        operator List<T, Allocator>::iteratorTemp<const value_type>() const;
    };

    typedef iteratorTemp<T> iterator;
    typedef iteratorTemp<const T> const_iterator;
    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
    const_iterator cbegin() const;
    const_iterator cend() const;

    reverse_iterator rbegin();
    reverse_iterator rend();
    const_reverse_iterator rbegin() const;
    const_reverse_iterator rend() const;
    const_reverse_iterator crbegin() const;
    const_reverse_iterator crend() const;

    template<typename vtype>
    iteratorTemp<vtype> insert(const iteratorTemp<vtype>&, const T&);

    template<typename vtype>
    void erase(const iteratorTemp<vtype>&);
};

template<typename T, typename Allocator>
template<typename vtype>
typename List<T, Allocator>::template iteratorTemp<vtype> &List<T, Allocator>::iteratorTemp<vtype>::operator++() {
    obj = obj ->next;
    return *this;
}

template<typename T, typename Allocator>
template<typename vtype>
typename List<T, Allocator>::template iteratorTemp<vtype> &List<T, Allocator>::iteratorTemp<vtype>::operator--() {
    obj = obj ->prev;
    return *this;
}

template<typename T, typename Allocator>
template<typename vtype>
typename List<T, Allocator>::template iteratorTemp<vtype> List<T, Allocator>::iteratorTemp<vtype>::operator++(int) {
    auto tmp = this;
    obj = obj ->next;
    return *tmp;
}

template<typename T, typename Allocator>
template<typename vtype>
typename List<T, Allocator>::template iteratorTemp<vtype> List<T, Allocator>::iteratorTemp<vtype>::operator--(int) {
    auto tmp = this;
    obj = obj ->prev;
    return *tmp;
}

template<typename T, typename Allocator>
template<typename vtype>
bool List<T, Allocator>::template iteratorTemp<vtype>::operator==(const List::iteratorTemp<vtype> &value) const {
    return obj == value.obj;
}

template<typename T, typename Allocator>
template<typename vtype>
bool List<T, Allocator>::template iteratorTemp<vtype>::operator!=(const List::iteratorTemp<vtype> &value) const {
    return obj != value.obj;
}

template<typename T, typename Allocator>
template<typename vtype>
vtype& List<T, Allocator>::iteratorTemp<vtype>::operator*() const {
    return reinterpret_cast<Node*>(obj) ->value;
}

template<typename T, typename Allocator>
template<typename vtype>
vtype* List<T, Allocator>::iteratorTemp<vtype>::operator->() const {
    return &reinterpret_cast<Node*>(obj) ->value;
}

template<typename T, typename Allocator>
template<typename vtype>
List<T, Allocator>::iteratorTemp<vtype>::operator List<T, Allocator>::iteratorTemp<const value_type>() const {
    return List::iteratorTemp<const value_type>(obj);
}

template<typename T, typename Allocator>
List<T, Allocator>::List(const Allocator &alloc):allocator_(typename std::allocator_traits<Allocator>::template rebind_alloc<Node>(alloc)),
                                                 allocator_m(alloc), size_(0), list(new NodeBase()) {
    connect(list, list);
}

template<typename T, typename Allocator>
List<T, Allocator>::List(size_t count, const Allocator &alloc):List(alloc) {
    size_ = count;
    while (count--) {
        Node* newNode = AllocatorTraits::allocate(allocator_, 1);
        AllocatorTraits::construct(allocator_, newNode);
        connect(list -> prev, newNode);
        connect(newNode, list);
    }
}

template<typename T, typename Allocator>
List<T, Allocator>::List(size_t count, const T &value, const Allocator &alloc):List(alloc) {
    while (count--) {
        push_back(value);
    }
}

template<typename T, typename Allocator>
List<T, Allocator>::List(const List &other):size_(0), list(new NodeBase()){
    connect(list, list);
    allocator_ = AllocatorTraits::select_on_container_copy_construction(other.allocator_);
    allocator_m = std::allocator_traits<Allocator>::select_on_container_copy_construction(other.allocator_m);
    auto it = other.begin();
    while (it != other.end()) {
        push_back(*it);
        ++it;
    }
}

template<typename T, typename Allocator>
List<T, Allocator>::List(List&& other) noexcept {
    size_ = std::move(other.size_);
    list = std::move(other.list);
    allocator_ = std::move(other.allocator_);
    allocator_m = std::move(other.allocator_m);
    other.size_ = 0;
    other.list = new NodeBase();
    connect(other.list, other.list);
}
template<typename T, typename Allocator>
List<T, Allocator> &List<T, Allocator>::operator=(const List &other) {
    if (this != &other) {
        while (size_)
            pop_back();
        allocator_ = AllocatorTraits::propagate_on_container_copy_assignment::value ? other.allocator_ : allocator_;
        allocator_m = std::allocator_traits<Allocator>::propagate_on_container_copy_assignment::value ?
                      other.allocator_m : allocator_m;
        auto it = other.begin();
        while (it != other.end()) {
            push_back(*it);
            ++it;
        }
    }
    return *this;
}

template<typename T, typename Allocator>
List<T, Allocator> &List<T, Allocator>::operator=(List&& other) noexcept {
    if (this != &other) {
        auto tmp = std::move(other);
        allocator_ = AllocatorTraits::propagate_on_container_move_assignment::value ? other.allocator_ : allocator_;
        allocator_m = std::allocator_traits<Allocator>::propagate_on_container_move_assignment::value ?
                      other.allocator_m : allocator_m;
        std::swap(list, tmp.list);
        std::swap(size_, tmp.size_);
    }
    return *this;
}


template<typename T, typename Allocator>
Allocator List<T, Allocator>::get_allocator() {
    return allocator_m;
}

template<typename T, typename Allocator>
size_t List<T, Allocator>::size() const{
    return size_;
}

template<typename T, typename Allocator>
void List<T, Allocator>::push_back(const T &elem) {
    Node* newNode = makeNewNode(elem);
    connect(list->prev, newNode);
    connect(newNode, list);
}

template<typename T, typename Allocator>
void List<T, Allocator>::push_front(const T &elem) {
    Node* newNode = makeNewNode(elem);
    connect(newNode, list->next);
    connect(list, newNode);
}

template<typename T, typename Allocator>
void List<T, Allocator>::pop_back() {
    Node* copy = reinterpret_cast<Node*>(list -> prev);
    connect(copy -> prev, list);
    eraseNode(copy);
}

template<typename T, typename Allocator>
void List<T, Allocator>::pop_front() {
    Node* copy = reinterpret_cast<Node*>(list -> next);
    connect(list, copy -> next);
    eraseNode(copy);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::Node *List<T, Allocator>::makeNewNode(const T& elem) {
    ++size_;
    Node* newNode = AllocatorTraits::allocate(allocator_, 1);
    AllocatorTraits::construct(allocator_, newNode, elem);
    return newNode;
}

template<typename T, typename Allocator>
void List<T, Allocator>::eraseNode(typename List<T, Allocator>::Node * node) {
    --size_;
    AllocatorTraits::destroy(allocator_, node);
    AllocatorTraits::deallocate(allocator_, node, 1);
}

template<typename T, typename Allocator>
void List<T, Allocator>::connect(NodeBase * ft, NodeBase * sd) {
    ft -> next = sd;
    sd ->prev = ft;
}


template<typename T, typename Allocator>
typename List<T, Allocator>::iterator List<T, Allocator>::begin() {
    return List::iterator(list -> next);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::iterator List<T, Allocator>::end() {
    return List::iterator(list);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::begin() const {
    return List::const_iterator(list -> next);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::end() const {
    return List::const_iterator(list);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::cbegin() const {
    return List::const_iterator(list -> next);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::cend() const {
    return List::const_iterator(list);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::reverse_iterator List<T, Allocator>::rbegin() {
    return List::reverse_iterator(end());
}

template<typename T, typename Allocator>
typename List<T, Allocator>::reverse_iterator List<T, Allocator>::rend() {
    return List::reverse_iterator(begin());
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::rbegin() const {
    return List::const_reverse_iterator(end());
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::rend() const {
    return List::const_reverse_iterator(begin());
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::crbegin() const {
    return List::const_reverse_iterator(cend());
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::crend() const {
    return List::const_reverse_iterator(cbegin());
}

template<typename T, typename Allocator>
template<typename vtype>
typename List<T, Allocator>::template iteratorTemp<vtype> List<T, Allocator>::insert(const List::iteratorTemp<vtype>& it, const T &val) {
    Node* node = makeNewNode(val);
    connect((it.obj) -> prev, node);
    connect(node, it.obj);
    return iteratorTemp<vtype>(node);
}

template<typename T, typename Allocator>
template<typename vtype>
void List<T, Allocator>::erase(const List::iteratorTemp<vtype>& it) {
    connect((it.obj) -> prev, (it.obj) -> next);
    eraseNode(reinterpret_cast<Node*>(it.obj));
}

template<typename T, typename Allocator>
List<T, Allocator>::~List() {
    auto it = --end();
    while (size() != 0) {
        auto copy = it;
        --it;
        erase(copy);
    }
    delete list;
}










//////////////////////////////////////////////__________________________________________________
//////////////////////////////////////////////________________________MAP_______________________
//////////////////////////////////////////////__________________________________________________
template<
        typename Key,
        typename Value,
        typename Hash = std::hash<Key>,
        typename KeyEqual = std::equal_to<Key>,
        typename Allocator = std::allocator< std::pair<const Key, Value> >
> class UnorderedMap {
public:
    using NodeType = std::pair<const Key, Value>;
    using UnorderedMapType = UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>;

    template<bool cnst>
    class iteratorTemplate {
    private:
        friend class UnorderedMap;
        using listIteratorType = //std::conditional_t<cnst, typename List<NodeType*, Allocator>::const_iterator,
        //typename List<NodeType*, Allocator>::iterator>;
        typename List<NodeType*, Allocator>::const_iterator;
        listIteratorType iterator_base;
    public:
        using iterator_category = std::forward_iterator_tag;
        using value_type = NodeType;
        using difference_type = size_t;
        using reference = std::conditional_t<cnst, const NodeType&, NodeType&>;
        using pointer = std::conditional_t<cnst, const NodeType*, NodeType*>;

        iteratorTemplate() = delete;
        iteratorTemplate(const listIteratorType& lt): iterator_base(lt){};
        ~iteratorTemplate() = default;
        //increment/decrement
        iteratorTemplate& operator++();
        iteratorTemplate operator++(int);
        bool operator==(const iteratorTemplate& other) const;
        bool operator!=(const iteratorTemplate& other ) const;

        reference operator*() const;
        pointer operator->() const;

        operator iteratorTemplate<true>();
    };
    using Iterator =  iteratorTemplate<false>;
    using ConstIterator = iteratorTemplate<true>;

    UnorderedMap();
    ~UnorderedMap();
    UnorderedMap(const UnorderedMapType& other);
    UnorderedMap(UnorderedMapType&& other) noexcept;
    UnorderedMapType& operator=(const UnorderedMapType& other);
    UnorderedMapType& operator=(UnorderedMapType&& other) noexcept;
    Value& operator[](const Key& key);
    Value& at(const Key& key);
    const Value& at(const Key& key) const;
    size_t size() const;
    Iterator find(const Key& key);


    Iterator begin();
    Iterator end();
    ConstIterator begin() const;
    ConstIterator end() const;
    ConstIterator cbegin() const;
    ConstIterator cend() const;
    size_t max_size() const;
    float load_factor() const;
    float max_load_factor() const;
    void rehash(size_t count);
    void reserve(size_t count);

    template<typename... Args>
    std::pair<Iterator, bool> emplace(Args&&... args);

    Iterator erase(ConstIterator it);
    Iterator erase(ConstIterator lt, ConstIterator rt);
    std::pair<Iterator, bool> insert(const NodeType& elem);

    template<typename Pair>
    std::pair<Iterator, bool> insert(Pair&& elem);

    template<typename InputIterator>
    void insert(InputIterator lt, InputIterator rt);

private:
    void update_if_needed();
    void swap_without_allocator(UnorderedMapType&& other);
    std::vector<typename List<NodeType*, Allocator>::const_iterator,
            typename std::allocator_traits<Allocator>
            ::template rebind_alloc<typename List<NodeType*, Allocator>::const_iterator>> hashtable;
    List<NodeType*, Allocator> list_main;
    float max_load_factor_val = 0.75;
    float load_factor_val = 0.0;

    Hash hash_function;
    KeyEqual is_equal;
    Allocator allocator;
public:

};


template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::UnorderedMap(): list_main() {
    hashtable.resize(1, list_main.end());
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::~UnorderedMap() {
    for (auto i = list_main.begin(); i != list_main.end(); ++i) {
        std::allocator_traits<Allocator>::destroy(allocator, (*i));
        std::allocator_traits<Allocator>::deallocate(allocator, (*i), 1);
    }
    hashtable.clear();
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::UnorderedMap(const UnorderedMapType& other):
        list_main(),
        hash_function(other.hash_function),
        is_equal(other.is_equal)
{
    allocator = std::allocator_traits<Allocator>::select_on_container_copy_construction(other.allocator);
    hashtable.resize(other.hashtable.size(), list_main.end());
    for (size_t i = 0; i < hashtable.size(); ++i)
        hashtable[i] = list_main.end();

    for (auto it = other.begin(); it != other.end(); ++it)
        insert(*it);
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::UnorderedMap(UnorderedMapType&& other) noexcept:
        hashtable(std::move(other.hashtable)),
        list_main(std::move(other.list_main)),
        load_factor_val(other.load_factor_val),
        hash_function(std::move(other.hash_function)),
        is_equal(std::move(other.is_equal)),
        allocator(std::move(other.allocator))
{
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
void UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::swap_without_allocator(
        UnorderedMap&& other) {
    hashtable = std::move(other.hashtable);
    list_main = std::move(other.list_main);
    load_factor_val = std::move(other.load_factor_val);
    hash_function = std::move(other.hash_function);
    is_equal = std::move(other.is_equal);
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>&
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::operator=(const UnorderedMapType &other) {
    if (this != &other) {
        auto copy = other;
        allocator = std::allocator_traits<Allocator>::propagate_on_container_copy_assignment::value ? other.allocator
                                                                                                    : allocator;
        swap_without_allocator(std::move(copy));
    }
    return *this;
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>&
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::operator=(UnorderedMapType &&other) noexcept{
    if (this != &other) {
        auto copy = std::move(other);
        allocator = std::allocator_traits<Allocator>::propagate_on_container_copy_assignment::value ? other.allocator
                                                                                                    : allocator;
        swap_without_allocator(std::move(copy));
    }
    return *this;
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
Value& UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::at(const Key& key) {
    auto it = find(key);
    if (it == end())
        throw std::out_of_range("");
    return it -> second;
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
const Value& UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::at(const Key& key) const {
    auto it = find(key);
    if (it == end())
        throw std::out_of_range("");
    return it -> second;
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
Value& UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::operator[](const Key& key) {
    auto it = find(key);
    if (it == end()) {
        auto res = insert({key, Value()});
        return res.first -> second;
    }
    else
        return it -> second;
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
size_t UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::size() const {
    return list_main.size();
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
typename UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::Iterator
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::find(const Key& key) {
    size_t index = hash_function(key) % hashtable.size();
    auto it = hashtable[index];
    while (it != list_main.end() && hash_function(((*it) -> first)) % hashtable.size() == index) {
        if (is_equal(key, (*it) -> first))
            return it;
        ++it;
    }
    return end();
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
size_t UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::max_size() const {
    return static_cast<size_t>(1e10);
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
float UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::load_factor() const {
    return list_main.size() / hashtable.size();
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
float UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::max_load_factor() const {
    return max_load_factor_val;
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
void UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::rehash(size_t count) {
    auto copy = std::move(list_main);
    hashtable.resize(count, list_main.end());
    for (size_t i = 0; i < hashtable.size(); ++i)
        hashtable[i] = list_main.end();

    for (auto it = copy.begin(); it != copy.end(); ++it) {
        auto head = hashtable[hash_function((*it) -> first) % hashtable.size()];
        if (head == list_main.end())
            hashtable[hash_function((*it) -> first) % hashtable.size()] =
                    list_main.insert(hashtable[hash_function((*it) -> first) % hashtable.size()], *it);
        else
            list_main.insert(hashtable[hash_function((*it) -> first) % hashtable.size()], *it);
    }
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
void UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::reserve(size_t count) {
    if (count <= hashtable.size())
        return;
    rehash(static_cast<size_t>(count / max_load_factor_val));
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
template<typename... Args>
std::pair<typename UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::Iterator, bool>
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::emplace(Args&&... args) {
    update_if_needed();
    NodeType* to_move = std::allocator_traits<Allocator>::allocate(allocator, 1);
    std::allocator_traits<Allocator>::construct(allocator, to_move, std::forward<Args>(args)...);
    if (to_move == nullptr)
        return {end(), false};
    auto it = find(to_move -> first);
    if (it != end())
        return {it, false};
    auto hash = hash_function(to_move -> first) % hashtable.size();
    return {Iterator(list_main.insert(hashtable[hash], to_move)), true};
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
typename UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::Iterator
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::erase(ConstIterator it) {
    if (it == cend())
        return end();
    auto origin = it++;
    auto hash = hash_function(origin -> first) % hashtable.size();
    if (hashtable[hash] == origin.iterator_base) {
        if (it == end())
            hashtable[hash] = list_main.end();
        else {
            auto hash_next = hash_function(it -> first) % hashtable.size();
            if (hash_next != hash)
                hashtable[hash] = list_main.end();
            else
                hashtable[hash] = it.iterator_base;
        }
    }
    std::allocator_traits<Allocator>::destroy(allocator, (*origin));
    std::allocator_traits<Allocator>::deallocate(allocator, (*origin), 1);
    list_main.erase(origin.iterator_base);
    return it.iterator_base;
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
typename UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::Iterator
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::erase(ConstIterator lt, ConstIterator rt) {
    for (;lt != rt; lt = erase(lt));
    return lt.iterator_base;
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
std::pair<typename UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::Iterator, bool>
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::insert(const NodeType& elem) {
    update_if_needed();
    auto it = find(elem.first);
    if (it != end())
        return {it, false};
    auto ptr = std::allocator_traits<Allocator>::allocate(allocator, 1);
    std::allocator_traits<Allocator>::construct(allocator, ptr, elem);
    auto head = hashtable[hash_function(elem.first) % hashtable.size()];
    head = list_main.insert(head, ptr);
    hashtable[hash_function(elem.first) % hashtable.size()] = head;
    return {head, true};
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
template<typename Pair>
std::pair<typename UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::Iterator, bool>
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::insert(Pair&& elem) {
    update_if_needed();
    auto it = find(elem.first);
    if (it != end())
        return {it, false};
    auto ptr = std::allocator_traits<Allocator>::allocate(allocator, 1);
    std::allocator_traits<Allocator>::construct(allocator, ptr, std::forward<Pair>(elem));
    auto head = hashtable[hash_function(elem.first) % hashtable.size()];
    head = list_main.insert(head, ptr);
    hashtable[hash_function(elem.first) % hashtable.size()] = head;
    return {Iterator(head), true};
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
template<typename InputIterator>
void UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::insert(InputIterator lt, InputIterator rt) {
    for (; lt != rt; ++lt)
        insert((*lt));
}


template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
template<bool cnst>
typename UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::template iteratorTemplate<cnst> &
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::iteratorTemplate<cnst>::operator++() {
    ++iterator_base;
    return *this;
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
template<bool cnst>
typename UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::template iteratorTemplate<cnst>
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::iteratorTemplate<cnst>::operator++(int) {
    auto copy = *this;
    ++iterator_base;
    return copy;
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
template<bool cnst>
bool UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::iteratorTemplate<cnst>::operator==(
        const UnorderedMap::iteratorTemplate<cnst> &other) const {
    return iterator_base == other.iterator_base;
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
template<bool cnst>
bool UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::iteratorTemplate<cnst>::operator!=(
        const UnorderedMap::iteratorTemplate<cnst> &other) const {
    return iterator_base != other.iterator_base;
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
template<bool cnst>
typename UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::template iteratorTemplate<cnst>::reference
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::iteratorTemplate<cnst>::operator*() const {
    return *(*iterator_base);
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
template<bool cnst>
typename UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::template iteratorTemplate<cnst>::pointer
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::iteratorTemplate<cnst>::operator->() const {
    return *iterator_base;
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
template<bool cnst>
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::iteratorTemplate<cnst>::operator
iteratorTemplate<true>() {
    return iteratorTemplate<true>(iterator_base);
}



template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
typename UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::ConstIterator
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::begin()  const{
    return ConstIterator(list_main.begin());
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
typename UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::ConstIterator
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::end()  const{
    return ConstIterator(list_main.end());
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
typename UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::Iterator
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::begin() {
    return Iterator(list_main.begin());
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
typename UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::Iterator
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::end() {
    return Iterator(list_main.end());
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
typename UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::ConstIterator
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::cbegin()  const{
    return ConstIterator(list_main.begin());
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
typename UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::ConstIterator
UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::cend()  const{
    return ConstIterator(list_main.end());
}

template<typename Key, typename Value, typename Hash, typename KeyEqual, typename Allocator>
void UnorderedMap<Key, Value, Hash, KeyEqual, Allocator>::update_if_needed() {
    if (load_factor() > max_load_factor())
        reserve(2 * hashtable.size() + 1);
}
