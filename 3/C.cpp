#include <iostream>
#include <vector>
#include <numeric>

int operations_cnt(std::vector<int> &numbers, int n) {
    int cnt = 0;
    for (int i = 0; i < n; ++i) {
        if (numbers[i] == 1) {
            ++cnt;
        }
    }
    if (cnt) {
        return n - cnt;
    }

    for (int i = n - 1; i >= 0; --i) {
        for (int j = 0; j < i; ++j) {
            numbers[j] = std::gcd(numbers[j], numbers[j + 1]);
            if (numbers[j] == 1) {
                return 2 * n - i - 1;
            }
        }
    }
    return -1;
}

int main() {
    int n;

    std::cin >> n;
    std::vector<int> numbers(n);

    for (auto &elem: numbers) {
        std::cin >> elem;
    }

    std::cout << operations_cnt(numbers, n);
    return 0;
}
