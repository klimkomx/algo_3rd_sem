#include <iostream>
#include <vector>
#include <cmath>


const long long MOD = 7340033;
const long long primitive_root = 5;

inline void input_optimization() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(0);
}

inline void norm(long long &val) {
    val = (val + MOD) % MOD;
}

long long bin_pow(long long num, long long pow, long long mod = MOD) {
    long long result = 1ll;
    while (pow) {
        if (pow & 1ll) {
            result = (result * num) % mod;
        }
        pow >>= 1;
        num = (num * num) % mod;
    }
    return result;
}

long long gcd_ext(long long a, long long b,
                  long long &ft, long long &sd) {
    if (a == 0ll) {
        ft = 0ll;
        sd = 1ll;
        return b;
    }
    long long tmp_ft,
            tmp_sd;
    auto res = gcd_ext(b % a, a, tmp_sd, tmp_ft);
    sd = tmp_sd;
    ft = (MOD + tmp_ft - ((b / a) * tmp_sd) % MOD) % MOD;
    return res;
}

long long reverse_elem(long long elem) {
    long long result, tmp;
    gcd_ext(elem, MOD, result, tmp);
    norm(result);
    return result;
}

int round_to_2_pow(int num) {
    int res;
    for (res = 1; res < num; res <<= 1);
    return res;
}

int pow_log(int num) {
    int res;
    for (res = 1; (1 << res) < num; ++res);
    return res;
}

void fast_fourier_transform(std::vector<long long> &polynom, bool is_reverse = false) {
    int size = static_cast<int>(polynom.size()),
            size_log = pow_log(size);
    long long root = bin_pow(primitive_root, ((MOD - 1ll) * reverse_elem(size)) % MOD),
            tmp;
    std::vector<int> reversed_ind(size);
    std::vector<long long> root_pows(size);

    if (is_reverse) {
        root = reverse_elem(root);
    }

    root_pows[0] = 1;
    for (int i = 1; i < size; ++i) {
        root_pows[i] = (root_pows[i - 1] * root) % MOD;
    }

    int position = -1;
    reversed_ind[0] = 0;
    for (int i = 1; i < size; ++i) {
        if (!(i & (i - 1))) {
            ++position;
        }
        reversed_ind[i] = reversed_ind[i ^ (1 << position)] |
                          (1 << (size_log - position - 1));
    }
    for (int i = 0; i < size; ++i) {
        if (reversed_ind[i] != i) {
            std::swap(polynom[i], polynom[reversed_ind[i]]);
            reversed_ind[reversed_ind[i]] = reversed_ind[i];
        }
    }

    for (int segm_size = 0; segm_size < size_log; ++segm_size) {
        for (int i = 0; i < size; i += (1 << (segm_size + 1))) {
            for (int j = 0; j < (1 << segm_size); ++j) {
                tmp = (root_pows[j * (1 << (size_log - segm_size - 1))]
                       * polynom[i + j + (1 << segm_size)]) % MOD;
                polynom[i + j + (1 << segm_size)] = (polynom[i + j] - tmp + MOD) % MOD;
                polynom[i + j] = (polynom[i + j] + tmp) % MOD;
            }
        }
    }
    return;
}

void reverse_fast_fourier_transform(std::vector<long long> &polynom) {
    fast_fourier_transform(polynom, true);
    auto inv_size = reverse_elem(polynom.size());

    for (auto &elem : polynom) {
        elem = (elem * inv_size) % MOD;
    }
}

int main() {
    input_optimization();

    int n, m, act_size;

    std::cin >> m >> n;
    act_size = 2 * round_to_2_pow(std::max(n + 1, m));
    std::vector<long long> a(act_size);

    for (int i = 0; i <= n; ++i) {
        std::cin >> a[i];
        norm(a[i]);
    }
    if (a[0] == 0) {
        std::cout << "The ears of a dead donkey";
        return 0;
    }

    std::vector<long long> b(act_size, 0);

    b[0] = reverse_elem(a[0]);
    fast_fourier_transform(a);

    for (int j = 1; j <= m; j *= 2ll) {

        fast_fourier_transform(b);
        for (int i = 0; i < act_size; ++i) {
            b[i] = (b[i] * ((MOD + 2 + (MOD - 1) * ((b[i] * a[i]) % MOD)) % MOD)) % MOD;
        }
        reverse_fast_fourier_transform(b);
        for (int i = (j << 1); i < act_size; ++i) {
            b[i] = 0;
        }
    }


    for (int i = 0; i < m; ++i) {
        std::cout << b[i] << " ";
    }
    std::cout << "\n";

    return 0;
}
