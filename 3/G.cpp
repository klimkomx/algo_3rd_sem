#include <iostream>
#include <vector>
#include <complex>
#include <cmath>

const double PI = acos(-1);

inline void input_optimization() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(0);
}


namespace helpers {
    void prepare_polynom(std::vector<std::complex<double> > &polynom,
                         const int size, const int size_log) {
        std::vector<int> reversed_ind(size);
        int position = -1;
        reversed_ind[0] = 0;

        for (int i = 1; i < size; ++i) {
            if (!(i & (i - 1))) {
                ++position;
            }
            reversed_ind[i] = reversed_ind[i ^ (1 << position)] |
                              (1 << (size_log - position - 1));
        }

        for (int i = 0; i < size; ++i) {
            if (reversed_ind[i] != i) {
                std::swap(polynom[i], polynom[reversed_ind[i]]);
                reversed_ind[reversed_ind[i]] = reversed_ind[i];
            }
        }
    }

    std::vector<std::complex<double> > get_root_pows(int size, bool is_reverse) {
        double arg = 2.0 * PI / size;
        std::complex<double> root(std::cos(arg), std::sin(arg));
        std::vector<std::complex<double> > pows(size + 1);

        if (is_reverse) {
            root = std::pow(root, -1.0);
        }

        pows[0] = 1;
        for (int i = 1; i < size; ++i) {
            pows[i] = pows[i - 1] * root;
        }
        return pows;
    }

    void fast_fourier_transform_base(std::vector<std::complex<double> > &polynom,
                                     bool is_reverse = false) {
        const int size = static_cast<int>(polynom.size()),
                size_log = static_cast<int>(std::log2(size));

        prepare_polynom(polynom, size, size_log);

        auto root_pows = get_root_pows(size, is_reverse);
        std::complex<double> tmp;

        for (int segm_size = 0; segm_size < size_log; ++segm_size) {
            for (int i = 0; i < size; i += (1 << (segm_size + 1))) {
                for (int j = 0; j < (1 << segm_size); ++j) {
                    tmp = root_pows[j * (1 << (size_log - segm_size - 1))]
                          * polynom[i + j + (1 << segm_size)];
                    polynom[i + j + (1 << segm_size)] = polynom[i + j] - tmp;
                    polynom[i + j] += tmp;
                }
            }
        }
        return;
    }

    std::vector<int> convert_and_cut_zeroes(const std::vector<std::complex<double> > &poly) {
        std::vector<int> result(poly.size());
        int last_non_zero_ind = -1;

        for (int i = 0; i < poly.size(); ++i) {
            result[i] = static_cast<int>(round(poly[i].real()));

            if (result[i] != 0) {
                last_non_zero_ind = i;
            }
        }
        result.resize(last_non_zero_ind + 1);
        return result;
    }
}

void fast_fourier_transform(std::vector<std::complex<double> > &polynom) {
    helpers::fast_fourier_transform_base(polynom, false);
}

void reverse_fast_fourier_transform(std::vector<std::complex<double> > &polynom) {
    helpers::fast_fourier_transform_base(polynom, true);
    auto size = static_cast<double>(polynom.size());

    for (auto &elem : polynom) {
        elem /= size;
    }
}


std::vector<int> multiply(const std::vector<int> &ft_original,
                          const std::vector<int> &sd_original) {
    int poly_pow = (1 << (static_cast<int>(std::log2(ft_original.size() +
                                                     sd_original.size())) + 1));
    std::vector<std::complex<double>> ft(poly_pow), sd(poly_pow);

    std::copy(ft_original.begin(), ft_original.end(), ft.begin());
    std::copy(sd_original.begin(), sd_original.end(), sd.begin());
    fast_fourier_transform(ft);
    fast_fourier_transform(sd);

    for (int i = 0; i < ft.size(); ++i) {
        ft[i] *= sd[i];
    }
    reverse_fast_fourier_transform(ft);

    return helpers::convert_and_cut_zeroes(ft);
}

void read(std::vector<int> &ft, std::vector<int> &sd, std::istream &in = std::cin) {
    int n, m;

    std::cin >> n;

    ft.resize(n + 1);

    for (int i = n; i >= 0; --i) {
        std::cin >> ft[i];
    }

    std::cin >> m;

    sd.resize(m + 1);

    for (int i = m; i >= 0; --i) {
        std::cin >> sd[i];
    }

}

void print(const std::vector<int> &poly, std::ostream &out = std::cout) {
    std::cout << poly.size() - 1 << " ";
    for (int i = poly.size() - 1; i >= 0; --i) {
        std::cout << poly[i] << " ";
    }
    std::cout << "\n";
}

int main() {
    input_optimization();

    std::vector<int> a, b;
    read(a, b);
    print(multiply(a, b));

    return 0;
}
