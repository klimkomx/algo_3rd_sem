#include <iostream>
#include <vector>

const long long MOD = 1e9 + 7;

inline void input_optimization() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(0);
}

long long bin_pow(long long num, long long pow, long long mod = MOD) {
    long long result = 1ll;
    while (pow) {
        if (pow & 1ll) {
            result = (result * num) % mod;
        }
        pow >>= 1;
        num = (num * num) % mod;
    }
    return result;
}

long long gcd_ext(long long a, long long b,
                  long long &ft, long long &sd, long long mod = MOD) {
    if (a == 0ll) {
        ft = 0ll;
        sd = 1ll;
        return b;
    }
    long long tmp_ft, tmp_sd;
    auto res = gcd_ext(b % a, a, tmp_sd, tmp_ft);
    sd = tmp_sd;
    ft = (tmp_ft - ((b / a) * tmp_sd + mod) % mod + 2 * mod) % mod;
    return res;
}

long long reverse(long long num, long long mod) {
    long long num_rev, tmp;
    gcd_ext(num, mod, num_rev, tmp);
    return num_rev;
}

long long stirlings_num(long long n, long long k, const std::vector<long long> &factorials,
                        const std::vector<long long> &factorials_rev, long long mod = MOD) {
    long long sum = 0ll,
            addition;

    for (long long i = 1; i <= k; ++i) {
        addition = bin_pow(i, n);
        addition = (addition * factorials[k]) % mod;
        addition = (addition * factorials_rev[i]) % mod;
        addition = (addition * factorials_rev[k - i]) % mod;

        if ((i + k) % 2ll == 1ll) {
            sum = (sum - addition + mod) % mod;
        } else {
            sum = (sum + addition) % mod;
        }
    }
    return (sum * factorials_rev[k]) % mod;
}

std::pair<std::vector<long long>,
        std::vector<long long> > count_factorials(long long max,
                                                  long long mod = MOD) {
    std::vector<long long> fact(max + 1), fact_rev(max + 1);
    fact[0] = fact_rev[0] = 1ll;
    fact[1] = fact_rev[1] = 1ll;
    for (long long i = 2; i <= max; ++i) {
        fact[i] = (i * fact[i - 1]) % mod;
        fact_rev[i] = (reverse(fact[i], mod) + mod) % mod;
    }
    return {fact, fact_rev};
}

long long sum(const std::vector<long long> &array, long long mod = MOD) {
    long long res = 0;
    for (auto elem : array) {
        res = (res + elem) % mod;
    }
    return res;
}

int main() {
    input_optimization();

    long long n, k;

    std::cin >> n >> k;

    std::vector<long long> weights(n);
    for (auto &elem : weights) {
        std::cin >> elem;
    }
    auto[fact, fact_rev] = count_factorials(k);

    std::cout << (((((n + k - 1ll) * stirlings_num(n - 1ll, k, fact, fact_rev)) % MOD
                    + stirlings_num(n - 1ll, k - 1ll, fact, fact_rev)) % MOD)
                  * sum(weights)) % MOD;

    return 0;
}
