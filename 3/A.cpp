#include <iostream>

const long long MOD = 1000000007;

inline void norm(long long &val) {
    val = (val + MOD) % MOD;
}

long long gcd_ext(long long a, long long b,
                  long long &ft, long long &sd) {
    if (a == 0ll) {
        ft = 0ll;
        sd = 1ll;
        return b;
    }
    long long tmp_ft,
            tmp_sd;
    auto res = gcd_ext(b % a, a, tmp_sd, tmp_ft);
    sd = tmp_sd;
    ft = tmp_ft - (b / a) * tmp_sd;
    return res;
}

int main() {

    long long a, b, c, d, bc_rev, tmp;

    std::cin >> a >> b >> c >> d;
    norm(a);
    norm(b);
    norm(c);
    norm(d);

    a = (a * d + b * c);
    norm(a);
    gcd_ext(b * d, MOD, bc_rev, tmp);
    norm(bc_rev);

    std::cout << (a * bc_rev) % MOD;
    return 0;
}
