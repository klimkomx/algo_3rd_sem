#include <iostream>
#include <vector>

int sieve_cnt(int n) {
    int cnt = 0;
    std::vector<int> primes;
    std::vector<int> min_divisor(n + 1, 0);

    for (int i = 2; i <= n; ++i) {
        if (min_divisor[i] == 0) {
            min_divisor[i] = i;
            primes.push_back(i);
        }
        for (int div : primes) {
            if (div > min_divisor[i] || div * i > n) {
                break;
            }
            cnt += div;
            min_divisor[i * div] = div;
        }
    }
    return cnt;
}

int main() {
    int n;

    std::cin >> n;

    std::cout << sieve_cnt(n);
    return 0;
}