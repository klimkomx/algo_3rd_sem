#include <iostream>
#include <vector>

const int MAX_VALUE = 2e6 + 1;

inline void input_optimization() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(0);
}

void cnt_divisors(std::vector<std::vector<int>> &divisors_array) {
    int max = static_cast<int>(divisors_array.size());
    std::vector<bool> is_prime(max, true);

    for (int i = 2; i < max; ++i) {
        if (is_prime[i]) {
            divisors_array[i].push_back(i);
            for (int j = 2 * i; j < max; j += i) {
                divisors_array[j].push_back(i);
                is_prime[j] = false;
            }
        }
    }
}

inline bool acceptable(const std::vector<int> &divisors,
                       const std::vector<bool> &used) {
    for (auto divisor : divisors) {
        if (used[divisor]) {
            return false;
        }
    }
    return true;
}

inline void fill(const std::vector<int> &divisors,
                 std::vector<bool> &used) {
    for (auto divisor : divisors) {
        used[divisor] = true;
    }
}


std::vector<int> find_array(const std::vector<int> &numbers, int size) {
    std::vector<std::vector<int>> divisors(MAX_VALUE);
    std::vector<bool> used(MAX_VALUE, false);
    std::vector<int> result(size);
    int iter = 0, iter_num;

    cnt_divisors(divisors);

    for (; iter < size; ++iter) {
        if (acceptable(divisors[numbers[iter]], used)) {
            result[iter] = numbers[iter];
            fill(divisors[numbers[iter]], used);
        } else {
            iter_num = numbers[iter] + 1;
            while (!acceptable(divisors[iter_num], used)) {
                ++iter_num;
            }
            result[iter] = iter_num;
            fill(divisors[iter_num], used);
            break;
        }
    }

    ++iter;
    iter_num = 2;

    for (; iter < size; ++iter) {
        while (!acceptable(divisors[iter_num], used)) {
            ++iter_num;
        }
        result[iter] = iter_num;
        fill(divisors[iter_num], used);
    }
    return result;
}

int main() {
    input_optimization();

    int n;

    std::cin >> n;
    std::vector<int> numbers(n);

    for (auto &elem : numbers) {
        std::cin >> elem;
    }

    auto answer = find_array(numbers, n);

    for (auto elem : answer) {
        std::cout << elem << " ";
    }
    std::cout << "\n";

    return 0;
}