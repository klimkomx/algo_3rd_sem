#include <vector>
#include <algorithm>
#include <cmath>
#include <iostream>
#include <utility>

const double PI = 3.1415926535,
        EPS = 1e-6;

inline double abs(double val) {
    return (val < 0) ? -val : val;
}

struct Point {
    double x, y;

    Point() = default;

    ~Point() = default;

    Point(double x, double y) : x(x), y(y) {}

    bool operator==(const Point &other) const {
        return (abs(x - other.x) < EPS && abs(y - other.y) < EPS);
    }

    bool operator!=(const Point &other) const {
        return !(*this == other);
    }

    bool operator<(const Point &other) const {
        return (x + EPS < other.x) ||
               (abs(x - other.x) < EPS && y + EPS < other.y);
    }

    bool operator>(const Point &other) const {
        return !(other < *this || other == *this);
    }
};

/////////////////Vector
struct Vector {
    double x, y;

    Vector() = default;

    ~Vector() = default;

    Vector(double x, double y) : x(x), y(y) {}

    Vector(const Point &ft, const Point &sd) :
            x(sd.x - ft.x), y(sd.y - ft.y) {}

    Vector(const Point &pt) : x(pt.x), y(pt.y) {}

    Vector &operator+=(const Vector &other) {
        x += other.x;
        y += other.y;
        return *this;
    }

    Vector &operator-=(const Vector &other) {
        x -= other.x;
        y -= other.y;
        return *this;
    }

    bool operator==(const Vector &other) const {
        return abs(x - other.x) + abs(y - other.y) < EPS;
    }

    bool operator!=(const Vector &other) const {
        return !(*this == other);
    }

    friend double dotProduct(const Vector &ft, const Vector &sd);

    friend double crossProduct(const Vector &ft, const Vector &sd);

    double length() const {
        return std::sqrt(x * x + y * y);
    }
};

double dotProduct(const Vector &ft, const Vector &sd) {
    return ft.x * sd.x + ft.y * sd.y;
}

double crossProduct(const Vector &ft, const Vector &sd) {
    return ft.x * sd.y - ft.y * sd.x;
}

Vector operator+(const Vector &ft, const Vector &sd) {
    Vector res = sd;
    return res += ft;
}

Vector operator-(const Vector &ft, const Vector &sd) {
    Vector res = sd;
    return res -= ft;
}

///////////////Line & Segment
class Line {
private:
    double a, b, c;
public:
    Line() = default;

    ~Line() = default;

    Line(const double &a, const double &b, const double &c) :
            a(a), b(b), c(c) {}

    Line(const Point &ft, const Point &sd) {
        a = ft.y - sd.y;
        b = sd.x - ft.x;
        c = crossProduct(ft, sd);
    }

    Vector directingVector() const {
        return Vector(-b, a);
    }

    bool is_parallel(const Line &other) const {
        return abs(crossProduct(directingVector(),
                                other.directingVector())) < EPS;
    }

    double dist(const Line &other) const {
        if (!is_parallel(other)) {
            return 0;
        }
        double scale = (abs(a) < EPS) ? (b / other.b) : (a / other.a);
        return abs(c - other.c * scale) / sqrt(a * a + b * b);
    }

    Point intersectionPoint(const Line &other) const {
        return Point((b * other.c - c * other.b) / (a * other.b - b * other.a),
                     -(a * other.c - c * other.a) / (a * other.b - b * other.a));
    }

    friend std::istream &operator>>(std::istream &in, Line &ln);

    friend std::ostream &operator<<(std::ostream &out, const Line &ln);
};

class Segment {
private:
    Point ft, sd;
public:
    Segment() = default;

    ~Segment() = default;

    Segment(const Point &a, const Point &b) : ft(a), sd(b) {}

    void normalize() {
        if (sd < ft) {
            std::swap(ft, sd);
        }
    }

    double length() const {
        return std::sqrt(abs(ft.x - sd.x) * abs(ft.x - sd.x) +
                         abs(ft.y - sd.y) * abs(ft.y - sd.y));
    }

    bool inSegment(const Point &pt) const {
        return abs(crossProduct(Vector(pt, ft), Vector(sd, ft))) < EPS &&
               std::min(ft.x, sd.x) < pt.x + EPS &&
               pt.x < std::max(ft.x, sd.x) + EPS &&
               std::min(ft.y, sd.y) < pt.y + EPS &&
               pt.y < std::max(ft.y, sd.y) + EPS;
    }

    bool __intersection_helper(const Segment &other) const {
        Vector middleVector(ft, sd),
                beginVector(ft, other.ft),
                endVector(ft, other.sd);
        if (crossProduct(beginVector, middleVector) * crossProduct(middleVector, endVector) > EPS) {
            return true;
        } else if (abs(crossProduct(beginVector, middleVector)) < EPS) {
            if (abs(crossProduct(middleVector, endVector)) < EPS) {
                if (std::max(ft.x, other.ft.x) <= std::min(sd.x, other.sd.x) &&
                    std::max(ft.y, other.ft.y) <= std::min(sd.y, other.sd.y)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } else if (abs(crossProduct(middleVector, endVector)) < EPS) {
            return true;
        } else {
            return false;
        }

    }

    bool intersect(const Segment &other) const {
        Line ftLine(ft, sd);
        Line sdLine(other.ft, other.sd);

        if (length() < EPS) {
            if (other.inSegment(ft)) {
                return true;
            } else {
                return false;
            }
        }
        if (other.length() < EPS) {
            if (inSegment(other.ft)) {
                return true;
            } else {
                return false;
            }
        }
        return __intersection_helper(other) && other.__intersection_helper(*this);
    }

    friend std::istream &operator>>(std::istream &in, Segment &ln);

    friend std::ostream &operator<<(std::ostream &out, const Segment &ln);

};

//////////////Polygon
class Polygon {
private:
    std::vector<Point> vertices;
public:
    Polygon(const std::vector<Point> &vt) : vertices(vt) {}

    ~Polygon() = default;

    bool inPolygon(const Point &pt) const {
        double angle = 0.0;
        for (int i = 0, j = 1; i < vertices.size(); ++i,
                j = (j + 1) % static_cast<int>(vertices.size())) {
            if (Segment(vertices[i], vertices[j]).inSegment(pt)) {
                return true;
            }
            Vector ft(pt, vertices[i]),
                    sd(pt, vertices[j]);
            angle += std::atan2(crossProduct(ft, sd), dotProduct(ft, sd));
        }
        return abs(angle) > EPS;
    }

    bool isConvex() const {
        bool rightOriented = false,
                leftOriented = false;
        for (int i = 0; i < vertices.size(); ++i) {
            Vector ft(vertices[i],
                      vertices[(i + 1) % static_cast<int>(vertices.size())]);
            Vector sd(vertices[(i + 1) % static_cast<int>(vertices.size())],
                      vertices[(i + 2) % static_cast<int>(vertices.size())]);
            double cp = crossProduct(ft, sd);
            if (cp + EPS < 0) {
                leftOriented = true;
            } else if (cp > EPS) {
                rightOriented = true;
            }
        }
        return rightOriented ^ leftOriented;
    }

    static Polygon convexHull(std::vector<Point> &pointsSet) {
        int idx = 0;
        for (int i = 1; i < pointsSet.size(); ++i) {
            if (pointsSet[i] < pointsSet[idx]) {
                idx = i;
            }
        }
        std::swap(pointsSet[0], pointsSet[idx]);
        Point ftPoint(pointsSet[0]);

        std::sort(pointsSet.begin() + 1, pointsSet.end(),
                  [&ftPoint](Point &a, Point &b) {
                      Vector ft(ftPoint, b);
                      Vector sd(ftPoint, a);
                      double cp = crossProduct(ft, sd);
                      return (cp + EPS < 0 ||
                              abs(cp) < EPS &&
                              sd.length() < ft.length());
                  });
        std::vector<Point> result = {pointsSet[0], pointsSet[1]};
        for (int i = 2; i < pointsSet.size(); ++i) {
            Point pt_now = pointsSet[i];
            while (result.size() >= 2 &&
                   crossProduct(Vector(result[result.size() - 2], result[result.size() - 1]),
                                Vector(result[result.size() - 1], pt_now)) + EPS >= 0) {
                result.pop_back();
            }
            result.push_back(pt_now);
        }
        return Polygon(result);
    }
};

std::istream &operator>>(std::istream &in, Point &pt) {
    in >> pt.x >> pt.y;
    return in;
}

std::ostream &operator<<(std::ostream &out, const Point &pt) {
    out << pt.x << " " << pt.y << "\n";
    return out;
}

std::istream &operator>>(std::istream &in, Vector &v) {
    in >> v.x >> v.y;
    return in;
}

std::ostream &operator<<(std::ostream &out, const Vector &v) {
    out << v.x << " " << v.y << "\n";
    return out;
}

std::istream &operator>>(std::istream &in, Line &ln) {
    in >> ln.a >> ln.b >> ln.c;
    return in;
}

std::ostream &operator<<(std::ostream &out, const Line &ln) {
    out << ln.a << " " << ln.b << " " << ln.c << "\n";
    return out;
}


std::istream &operator>>(std::istream &in, Segment &sg) {
    in >> sg.ft >> sg.sd;
    sg.normalize();
    return in;
}

std::ostream &operator<<(std::ostream &out, const Segment &sg) {
    out << sg.ft << sg.sd;
    return out;
}

int main() {
    std::cin.tie(0);
    std::ios::sync_with_stdio(false);

    int n;
    Point pt;

    std::cin >> n >> pt;

    std::vector<Point> points(n);

    for (auto &point : points) {
        std::cin >> point;
    }

    Polygon poly(points);
    std::cout << ((poly.inPolygon(pt)) ? "YES" : "NO");
    return 0;
}