#include <vector>
#include <algorithm>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <utility>

const double EPS = 1e-9;

template<typename T>
inline T abs(const T &val) {
    return (val < 0) ? -val : val;
}

class ComparatorForDouble {
public:
    static bool comp(const double &ft, const double &sd) {
        return ft + EPS < sd;
    }

    static bool equiv(const double &ft, const double &sd) {
        return abs(ft - sd) < EPS;
    }
};

class ComparatorForLong {
public:
    static bool comp(const long long &ft, const long long &sd) {
        return ft < sd;
    }

    static bool equiv(const long long &ft, const long long &sd) {
        return ft == sd;
    }
};

template<typename T, typename Compare>
struct Point {
    T x, y;

    Point() = default;

    ~Point() = default;

    Point(T x, T y) : x(x), y(y) {}

    bool operator==(const Point &other) const {
        return Compare::equiv(x, other.x) &&
               Compare::equiv(y, other.y);
    }

    bool operator!=(const Point &other) const {
        return !(*this == other);
    }

    bool operator<(const Point &other) const {
        return Compare::comp(x, other.x) ||
               (Compare::equiv(x, other.x) && Compare::comp(y, other.y));
    }

    bool operator>(const Point &other) const {
        return !(other < *this || other == *this);
    }
};

/////////////////Vector
template<typename T, typename Compare>
struct Vector {
    T x, y;

    Vector() = default;

    ~Vector() = default;

    Vector(T x, T y) : x(x), y(y) {}

    Vector(const Point<T, Compare> &ft, const Point<T, Compare> &sd) :
            x(sd.x - ft.x), y(sd.y - ft.y) {}

    Vector(const Point<T, Compare> &pt) : x(pt.x), y(pt.y) {}

    Vector &operator+=(const Vector &other) {
        x += other.x;
        y += other.y;
        return *this;
    }

    Vector &operator-=(const Vector &other) {
        x -= other.x;
        y -= other.y;
        return *this;
    }

    bool operator==(const Vector &other) const {
        return Compare::equiv(x, other.x) &&
               Compare::equiv(y, other.y);
    }

    bool operator!=(const Vector &other) const {
        return !(*this == other);
    }

    template<typename U, typename CompareU>
    friend U dotProduct(const Vector<U, CompareU> &ft, const Vector<U, CompareU> &sd);

    template<typename U, typename CompareU>
    friend U crossProduct(const Vector<U, CompareU> &ft, const Vector<U, CompareU> &sd);

    double length() const {
        return std::sqrt(x * x + y * y);
    }
};


template<typename T, typename Compare>
T dotProduct(const Vector<T, Compare> &ft, const Vector<T, Compare> &sd) {
    return ft.x * sd.x + ft.y * sd.y;
}

template<typename T, typename Compare>
T crossProduct(const Vector<T, Compare> &ft, const Vector<T, Compare> &sd) {
    return ft.x * sd.y - ft.y * sd.x;
}

template<typename T, typename Compare>
Vector<T, Compare> operator+(const Vector<T, Compare> &ft, const Vector<T, Compare> &sd) {
    Vector res = sd;
    return res += ft;
}

template<typename T, typename Compare>
Vector<T, Compare> operator-(const Vector<T, Compare> &ft, const Vector<T, Compare> &sd) {
    Vector res = sd;
    return res -= ft;
}

//////////////Polygon
template<typename T, typename Compare>
class Polygon {
public:
    Polygon(const std::vector<Point<T, Compare> > &vt) : vertices(vt) {}

    ~Polygon() = default;

    size_t size() const {
        return vertices.size();
    }

    static Polygon convexHull(std::vector<Point<T, Compare> > pointsSet) {
        int idx = 0;
        for (int i = 1; i < pointsSet.size(); ++i) {
            if (pointsSet[i] < pointsSet[idx]) {
                idx = i;
            }
        }
        std::swap(pointsSet[0], pointsSet[idx]);
        Point<T, Compare> ftPoint(pointsSet[0]);

        std::sort(pointsSet.begin() + 1, pointsSet.end(),
                  [&ftPoint](Point<T, Compare> &a, Point<T, Compare> &b) {
                      Vector<T, Compare> ft(ftPoint, a);
                      Vector<T, Compare> sd(ftPoint, b);
                      T cp = crossProduct(ft, sd);
                      return (Compare::comp(0, cp) ||
                              Compare::equiv(cp, 0) &&
                              ft.length() < sd.length());
                  });
        std::vector<Point<T, Compare> > result = {pointsSet[0], pointsSet[1]};
        for (int i = 2; i < pointsSet.size(); ++i) {
            Point<T, Compare> pt_now = pointsSet[i];
            while (result.size() >= 2 &&
                   crossProduct(Vector<T, Compare>(result[result.size() - 2], result[result.size() - 1]),
                                Vector<T, Compare>(result[result.size() - 1], pt_now)) <= 0) {
                result.pop_back();
            }
            result.push_back(pt_now);
        }
        return Polygon(result);
    }

    template<typename U, typename CompareU>
    friend std::ostream &operator<<(std::ostream &out, const Polygon<U, CompareU> &poly);

private:
    std::vector<Point<T, Compare> > vertices;
};

template<typename T, typename Compare>
std::istream &operator>>(std::istream &in, Point<T, Compare> &pt) {
    in >> pt.x >> pt.y;
    return in;
}

template<typename T, typename Compare>
std::ostream &operator<<(std::ostream &out, const Point<T, Compare> &pt) {
    out << pt.x << " " << pt.y << "\n";
    return out;
}

template<typename T, typename Compare>
std::ostream &operator<<(std::ostream &out, const Polygon<T, Compare> &poly) {
    for (const auto &point : poly.vertices) {
        out << std::setprecision(16) << point;
    }
    return out;
}

using PolygonInt = Polygon<long long, ComparatorForLong>;
using PointInt = Point<long long, ComparatorForLong>;

int main() {
    std::cin.tie(0);
    std::ios::sync_with_stdio(false);

    int n;

    std::cin >> n;

    std::vector<PointInt> points(n);

    for (auto &point : points) {
        std::cin >> point;
    }

    PolygonInt hull = PolygonInt::convexHull(points);

    std::cout << hull.size() << "\n" << hull;

    return 0;
}